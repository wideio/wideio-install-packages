// Note : can be very easily generalized to any dataset stored on local disk with hierarchical structure
using System;
using System.Diagnostics;
using System.Collections;
using System.IO;
using System.Text;
using System.Collections.Generic;
using System.ServiceModel.Web;           // for DataContractJsonSerializer. Need reference too.
using System.Runtime.Serialization.Json; // for DataContractJsonSerializer
using System.Text.RegularExpressions;
using System.Reflection;


/*

Documentation
========

*HierarchicalFileDataset* is a class that implements most of things needed to implement IDataset interface for arbitrary dataset that is stored on disk
Files should be organised in folders, files starting with dot (.) will be omitted

Meta files 
--------------

*to add meta data write .meta file anywhere. Exemplary meta file would be :
*[{"Key":"20130323_090710.jpg","Value":{"color":"Vivalto"}},
{"Key":"20130323_090717.jpg","Value":{"color":"Volluto"}},
{"Key":"20130323_090726.jpg","Value":{"color":""}},
{"Key":"20130323_090735.jpg","Value":{"color":"Finezzo"}},
{"Key":"20130323_090753.jpg","Value":{"color":"Volluto"}},
{"Key":"20130323_090800.jpg","Value":{"color":"Volluto"}},
{"Key":"20130323_090810.jpg","Value":{"color":"Ristretto"}},
{"Key":"20130323_090828.jpg","Value":{"color":"Capriccio"}},
{"Key":"20130323_090854.jpg","Value":{"color":""}}
]

Where Key is file name (it will assign MetaData to *all* files with this file in directory of .meta so if you create .meta with
[{"Key":"test.jpg", "Value":{"test_attribute1":10, "test_attribute2":"abc"}]
 at the root directory it will assign MetaData to all test.jpg in your data set) and Value is Dict (should contain same fields name as in MetaData class to serialize correctly)
 
 
Inheriting

----------
Every dataset should be reflected in a class inheriting after HierarchicalFileDataset

1. Define statics
	-- regex_name : regex used to parse folder names, for trivial parsing set to .* (another example, ([^\s]*)(.*), with group = {0} would take first word)
	-- groups_name : which groups from regex should go into parsed folder name (example {0} - it is an int array)


2. Implement MetaLabel class should have field named "physicalPath" (exactly that name, imporant for reflection). This class should be called "MetaLabel" also exactly

3. Implement meta data getters like this
[WideIO.DatasetTypeMetaData(typeof(String))]     
		public object GetMetadataColor(object id){ 
			try{
				foreach(var x in this.dataElements){
					if(x.ID == (string)id){
						return x.metaLabel.color; //new MetaLabel{color = x.metaLabel.color, physicalPath = x.metaLabel.physicalPath}; //add reflection code here
					}
				}		
				return null;
			}
			catch(Exception){
				//silent exception
				return null;
			}
		}
attribute decorator is important for parser

4. Implement GetElement(id) 

(see example)
*/


//TODO : test .meta at root

//TODO: for real performance code : composite pattern (search tree), because having dicts will be still much slower, but for now it will sufice

//TODO: add logger like registering calls

//TODO: add casting syntactic sugar in DataEntry (idea: Cast<T> using reflection and getter + setter)

//TODO: refactor MetaData ---> MetaLabel

//TODO: make better serializer (using reflection and fieldinfo[])

namespace wideiogenericdatasets
{

	/// <summary>
	/// Hierarchical file dataset generic class
	/// </summary>
	public abstract class HierarchicalFileDataset : wideiogenericdatasets.IDataset
	{
		// ======= Fields  ======== //
		protected static string DEFAULT_ROOT_PATH = "./XloudiaColors";
		protected static int[] GROUPS_NAME = {1}; 
		protected static string REGEX_NAME = "([^\\s]*)(.*)"; //for ignoring parts of folder name (you could take for instance only first 3 letters)
	
		
		protected List<DatasetEntry> dataElements =new List<DatasetEntry>();
		protected Dictionary<string, DatasetEntry> idDict = new Dictionary<string, DatasetEntry>();
		protected Dictionary<string, List<DatasetEntry>> basenameDict = new Dictionary<string, List<DatasetEntry>>(); //can be non unique potentially		
		
		/// <summary>
		/// Parameters class for the Dataset
		/// </summary>
		public class Parameters{
			public string root_path = "./XloudiaColors"; // by default in the same folder
		}		
		
		public HierarchicalFileDataset(){ 
			this.parameters = base.DefaultParameters();
			(this.parameters as Parameters).root_path = HierarchicalFileDataset.DEFAULT_ROOT_PATH;
		}
		
		
		//mozna by sie tego pozbyc.
		
		/// <summary>
		/// MetaLabel stored in folder and associated with each file
		/// </summary>
	
		/// <summary>
		/// What is stored in the dataset
		/// </summary>
		public class DatasetEntry{
			// ID is simply a list of string
			public string ID;
			public object metaLabel;
			//TODO: think about better ID and rewrite this function
			public string getBaseName(){
				//TODO: add caching (getter setter syntax)
				string [] tokens = ID.Split('/');
				return tokens[tokens.Length-1];
			}
		}
		
		
		
		// ============= IDataset methods ================ // 
		
		public override IEnumerable ids(){	
			foreach(var x in this.dataElements)
				yield return new string(x.ID.ToCharArray()); //add reflection here
		}
		
		public override void UpdateParameters(object p){
			object backup = this.parameters;
			try{
				this.parameters = (Parameters)p; // failed cast will throw an exception
			}
			catch(Exception e){
				this.parameters = backup;
				Console.WriteLine("ERROR: Wrong parameters supplied, parameters not affected") ; //TODO: add logger
				Console.WriteLine(e.ToString());
			}		
			this.LoadDataset();
		}
		

		
		// ================ private ======================= //
		// parse meta labels in directory after adding entries
		// if in root directory then all files with given names got assigned, if in local only those in the directory
		protected void ParseMetaLabels(string dir_name, string hierarchy){
			
			Type metaType = this.GetType().GetNestedType("MetaLabel");
			
			try{				
				string meta_data_jsons = File.ReadAllText(dir_name+"/.meta");
				Console.WriteLine("Meta for "+dir_name);
				Console.WriteLine(meta_data_jsons);
				
			
				
				object dict = (object) typeof(Utils).GetMethod ("deserialize_object")
							.MakeGenericMethod(typeof(Dictionary<,>).MakeGenericType(typeof(String), metaType)  )
					     	.Invoke(null, new object[]{(object)meta_data_jsons}); //magic.
				
				PropertyInfo dict_count = typeof(Dictionary<,>).MakeGenericType(typeof(String), metaType).GetProperty("Count");
				
				Console.WriteLine("Dict count is "+dict_count.GetValue(dict, null));
				
				PropertyInfo dict_keys = typeof(Dictionary<,>).MakeGenericType(typeof(String), metaType).GetProperty("Keys");
				
				MethodInfo dict_get_value =  typeof(Dictionary<,>).MakeGenericType(typeof(String), metaType).GetMethod("get_Item");
				
	
				//Dictionary<object,object> dict2;
				
				//	Utils.deserialize_object<Dictionary<string, MetaLabel>>(meta_data_jsons);
				foreach(var k in (IEnumerable)dict_keys.GetValue(dict,null)) {
					List<DatasetEntry> list_x = null;
					this.basenameDict.TryGetValue((string)k, out list_x);
					foreach(var x in list_x){
						if(x.getBaseName() == (string)k && x.ID.StartsWith(hierarchy)){

							FieldInfo[] fields = metaType.GetFields();
							foreach(var f in fields)
								if(f.GetValue(dict_get_value.Invoke(dict, new object[]{k})) != null)
									f.SetValue(x.metaLabel, f.GetValue(dict_get_value.Invoke(dict, new object[]{k})));								
						}
					}
				}
			}
			catch(Exception e){
				Console.WriteLine("exp");
				if(e is FileNotFoundException) return; // normal error, not every dir has to have .meta
				Console.WriteLine("ERROR : in parsing meta labels "+e.ToString() +" file path = "+dir_name);
			}
		}
		
		
		

		// TODO: Improve speed (precompile regex)
			
		protected string ParseDirectoryName(string dir_name){
			string output="";
			int counter =0 ;
			foreach (Match match in Regex.Matches(dir_name, HierarchicalFileDataset.REGEX_NAME)){
				foreach(var x in match.Groups){				
					if(Array.Exists(HierarchicalFileDataset.GROUPS_NAME, el => el == counter)) {
						output += x.ToString();
					}
					counter++;
				}
				
			}
			return output;
		}
		/// <summary>
		/// Scans the files in provided path and replaces currently stored data
		/// </summary>
		/// 
		protected void RecursiveScanFiles(string path, string hierarchy){
			Type metaType = this.GetType().GetNestedType("MetaLabel");
			foreach (string f in Directory.GetFiles(path)) {
						string[] tokens = f.Split('/'); 	
						string ID = hierarchy;
						if(tokens[tokens.Length-1].StartsWith(".")) continue; //ignore files starting with dot
						ID+=(tokens[tokens.Length-1]);
					
						DatasetEntry dse = new DatasetEntry{ID=ID, metaLabel = Activator.CreateInstance(metaType)};     
					    	                        
					    FieldInfo physical_path_field = metaType.GetField("physicalPath");
						physical_path_field.SetValue (typeof(Utils).GetMethod ("Cast")
					              .MakeGenericMethod(metaType)
					              .Invoke (null,new object[]{dse.metaLabel}), f);
					
						this.dataElements.Add (dse);
			}
			foreach (string d in Directory.GetDirectories(path)) {
					string inherited_hierarchy = hierarchy;
					string [] tokens = d.Split('/'); 					
					inherited_hierarchy += (this.ParseDirectoryName(tokens[tokens.Length-1]))+"/";
					this.RecursiveScanFiles(d, inherited_hierarchy);
			}		
		}
		protected void RecursiveParseMeta(string path, string hierarchy){
			//Console.WriteLine("level = "+hierarchy);
			Console.WriteLine(path);
			foreach (string d in Directory.GetDirectories(path)) {
				Console.WriteLine(d);
					string inherited_hierarchy = hierarchy;
					//TODO: can be done more optimally pushing and poping given element
					string [] tokens = d.Split('/'); 					
					inherited_hierarchy += (this.ParseDirectoryName(tokens[tokens.Length-1]))+"/";
					this.ParseMetaLabels( d, inherited_hierarchy);
			}		
		}		
		protected void LoadDataset(){
			Console.WriteLine(">> Scanning files after update..");	
			List<DatasetEntry> dataElementsBackup = this.dataElements;
			try {
				Console.WriteLine(">> RecursiveScanFiles..");
				RecursiveScanFiles((this.parameters as Parameters).root_path, "");
				this.idDict.Clear();
				Console.WriteLine(">> Building Global Dicts..");
				foreach(var x in this.dataElements) this.idDict[x.ID] = x;
				foreach(var x in this.dataElements) {
					if(!this.basenameDict.ContainsKey(x.getBaseName()))this.basenameDict[x.getBaseName()] = new List<DatasetEntry>();		
					this.basenameDict[x.getBaseName()].Add(x);
				}
				Console.WriteLine(">> RecursiveParseMeta..");
				this.RecursiveParseMeta((this.parameters as Parameters).root_path,"");
			}
			catch(Exception e){
				Console.WriteLine("ERROR: in scanning directory");
				Console.WriteLine(e.ToString());
				
				this.dataElements = dataElementsBackup; //gb will take care
			}
			
			
		}

	
	}
	

	
	/// <summary>
	/// X loudia colors dataset - exemplary usage
	/// </summary>
	[DatasetAnnotation]
	public class XLoudiaColorsDataset: HierarchicalFileDataset{
		
		public class MetaLabel{
			// NOTE: make sure that every type except *string* has a default constructor
			public string color = "";
			public string test = "";
			public string physicalPath = "";
			public override string ToString ()
			{
				return test + "::" + color + "::" + physicalPath;
			}
		}
							
		[DatasetTypeAttribute(typeof(MetaLabel))]
		public object GetMetadataBundle(object id){ 
			try{
				foreach(var x in this.dataElements){
					if(x.ID == (string)id){
						return new MetaLabel{color = ((MetaLabel)x.metaLabel).color, physicalPath = ((MetaLabel)x.metaLabel).physicalPath}; //add reflection code here
					}
				}		
				return null;
			}
			catch(Exception){
				//silent exception
				return null;
			}
		}	
				
		[DatasetTypeAttribute(typeof(System.Drawing.Image))]     
		public override object GetElement(object id){ 
			//TODO: add a dictionary instead of linear scan
			//TODO: how to handle if there is no object with given id
			foreach(var x in this.dataElements){
				if(x.ID == (string)id){
					return System.Drawing.Image.FromFile((x.metaLabel as MetaLabel).physicalPath);
				}
			}
			return null;
		}

		
	}
	

	
	/// <summary>
	/// Singapour bilets dataset - exemplary usage
	/// </summary>
	[DatasetAnnotation]
	public class SingapourBiletsDataset: HierarchicalFileDataset{
		
		public class MetaLabel{
			// NOTE: make sure that every type except *string* has a default constructor
			public string physicalPath = "";
			public override string ToString ()
			{
				return  physicalPath;
			}
		}
							
		[DatasetTypeAttribute(typeof(MetaLabel))]
		public object GetMetadataBundle(object id){ 
			try{
				foreach(var x in this.dataElements){
					if(x.ID == (string)id){
						return new MetaLabel{physicalPath = ((MetaLabel)x.metaLabel).physicalPath}; //add reflection code here
					}
				}		
				return null;
			}
			catch(Exception){
				//silent exception
				return null;
			}
		}	
				
		[DatasetTypeAttribute(typeof(System.Drawing.Image))]     
		public override object GetElement(object id){ 
			//TODO: add a dictionary instead of linear scan
			//TODO: how to handle if there is no object with given id
			foreach(var x in this.dataElements){
				if(x.ID == (string)id){
					return System.Drawing.Image.FromFile((x.metaLabel as MetaLabel).physicalPath);
				}
			}
			return null;
		}

		
	}
		
	
	
}

