using System;
using System.Diagnostics;
using System.Collections;
using System.IO;
using System.Text;
using System.Collections.Generic;
using System.ServiceModel.Web;           // for DataContractJsonSerializer. Need reference too.
using System.Runtime.Serialization.Json; // for DataContractJsonSerializer
using System.Linq;
using Newtonsoft.Json;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Imaging;


namespace WIODatasetLanguage
{
	public static class Utils
	{

		public static Gdk.Pixbuf ImageToPixbuf(System.Drawing.Image image)
		{
			using (MemoryStream stream = new MemoryStream())
			{
				image.Save(stream, ImageFormat.Bmp);
				stream.Position = 0;
				Gdk.Pixbuf pixbuf = new Gdk.Pixbuf(stream);
				return pixbuf;
			}
		}

		public static T Cast<T>(object o)
        {
            return (T)o;
        }
		
		public static void SerializeCollection<T>( IEnumerable<T> collection){
			string output = "";
			foreach(var x in collection) output+=x.ToString()+",";
			output += "\n";
		}		
		public static void WriteCollection<T>( IEnumerable<T> collection){
			foreach(var x in collection) Console.Write(x.ToString()+",");
			Console.Write("\n");
		}
		/// <summary>
		/// Clone the specified collection , WARNING : can be possibly a shallow copy!!
		/// </summary>
		public static IEnumerable<T> Clone<T>( IEnumerable<T> collection) where T : ICloneable
		{
			return collection.Select (item => (T)item.Clone ());
		}
		/// <summary>
		/// Serialize_object util function
		/// </summary>
		/// <param name='obj'>
		/// Object to be serialized
		/// </param>
		public static string serialize_object(Object obj){
			string serialization = "";
			DataContractJsonSerializer serializer = new DataContractJsonSerializer(obj.GetType());
			MemoryStream ms = new MemoryStream();
			serializer.WriteObject(ms, obj);
			serialization = Encoding.Default.GetString(ms.ToArray());
			ms.Close();
			return serialization;
		}
		
		/// <summary>
		/// Deserialize_object util function
		/// </summary>
		/// <param name='obj'>
		/// String of deserialized object
		/// </param>
		public static T deserialize_object<T>(string serialization) where T: class{
			DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(T));
			
			MemoryStream ms = new MemoryStream(Encoding.Default.GetBytes(serialization));
			T x = serializer.ReadObject(ms) as T;
			ms.Close();
			return x;
		}


	}
}

