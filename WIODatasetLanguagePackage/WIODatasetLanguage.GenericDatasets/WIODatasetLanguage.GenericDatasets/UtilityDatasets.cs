using System;
using System.Diagnostics;
using System.Collections;
using System.IO;
using System.Text;
using System.Collections.Generic;
using System.ServiceModel.Web;           // for DataContractJsonSerializer. Need reference too.
using System.Runtime.Serialization.Json; // for DataContractJsonSerializer
using System.Text.RegularExpressions;
using System.Reflection;
using System.Linq;
using WIODataLanguage;


//TODO: improve return new tuple to match exactly (querytype)
//TODO: look into TryRemove in each dataset class

//TODO: collapse code creating idListTmp
//TODO: add capacity to removeBatch
//TODO: add support for FullName

//Count
//GetAvailableMetadata
//GetMetaByLabel

namespace WIODatasetLanguage
{
	//mongo
	//explode
	//bang
	//whole
	//trainingset
	//testset


	/// <summary>
	/// Union utility dataset. Imporant note : ID in dataset unioned has to be cloneable.
	/// </summary>
	[WIOCallnameAnnotation("Concat")]
	public class ConcatDataset: WIODataLanguage.IDataset{
		//it is imporant for ID to hashable and serailizable easily
		//I have picked tuple<int, object> for ID because there no guarantees for distinct IDs (especially if we are making an operation with a simple database)
		
		

		
		
		protected List<MyID> idList = new List<MyID>();
		
		/// <summary>
		/// Note : Union class takes complete ownership of proxy dataset objects
		/// </summary>
		public ConcatDataset(){
		}

		//that's all we can say, it is either of two types
		[WIODataLanguage.DatasetTypeAttribute(typeof(object))]     
		public override object GetElement (object id)
		{
			MyID casted_id = id as MyID;

			if (casted_id == null) 
				throw new WIODataLanguageWrongFormatException (); 

			return casted_id.Item1 == 1 ? ((Parameters)(GetParameters())).dataset1.GetElement (casted_id.Item2) : 
				((Parameters)(GetParameters())).dataset2.GetElement (casted_id.Item2);
		}


		public override IEnumerable Keys ()
		{
			//note : it cannot be foreach because Clone modifies enumeration.. idk how to solve it
			int idListSize = this.idList.Count; // otherwise it will count it every iteration (really!:)
			for (int i=0; i< idListSize ;++i)
				yield return this.idList[i].Clone();
		}
		public override void UpdateParameters (object parameters)
		{
			this.parameters = parameters;//new Parameters{dataset1 = ((Parameters)parameters).dataset1, dataset2 = ((Parameters)parameters).dataset2}; //to be on the safe side, btw : 
			//because for caching we need highly standarized datasets, we can for instance 
			//order dataset1 and dataset2 by calling name (attribute)

			//Reset
			this.idList.Clear();

			//parameters should be a struct to be copyable easily but we need inheritance from time to time
			this.MergeDatasets();
		}

		
		public override object GetMetaByLabel (object id, string name)
		{
			MyID casted_id = this.CastId (id);
			if (casted_id == null) { //that has to be checked 
				throw new WIODataLanguageWrongFormatException (); 
			}	
			return casted_id.Item1 == 1 ? ((Parameters)(GetParameters())).dataset1.GetMetaByLabel (casted_id.Item2, name) :
				((Parameters)(GetParameters())).dataset2.GetMetaByLabel (casted_id.Item2, name);
			
		}


			
		public override bool TryRemoveElementEntries (object [] ids)
		{
	
			Dictionary<object, bool> removeDict = new Dictionary<object, bool>();
			foreach(object id in ids) removeDict[id] = true;
			
			int idListRemove = this.idList.RemoveAll(ele => removeDict.ContainsKey(ele)); //O(N) but batch!!
			bool removeDataset = false;
			
			object[] ids1 = (from el in ids where ((MyID)el).Item1==1 select ((MyID)el).Item2).ToArray(); 
			object[] ids2 = (from el in ids where ((MyID)el).Item1==2 select ((MyID)el).Item2).ToArray();
			((Parameters)(GetParameters())).dataset1.TryRemoveElementEntries(ids1);
			((Parameters)(GetParameters())).dataset2.TryRemoveElementEntries(ids2);
			
			
			return  (idListRemove>0) && removeDataset;	
		}
		
		
		
		/// <summary>
		/// Gets available metadata (idea is that not every object has to have all the metadata present)
		/// </summary>
		/// <returns>
		/// The available metadata in string[] (simplicity for parser)
		/// </returns>
		public override string[] GetAvailableMetadata (object id = null)
		{
		

			MyID casted_id = this.CastId (id);
			
			List<string> merged_metadata_labels = new List<string> ();
			
			if (id != null) { //query about specific item	
				if (casted_id.Item1 == 1)
					merged_metadata_labels.AddRange (((Parameters)parameters).dataset1.GetAvailableMetadata (casted_id.Item2));
				else 
					merged_metadata_labels.AddRange (((Parameters)parameters).dataset2.GetAvailableMetadata (casted_id.Item2));		
			}
			else{
				
				merged_metadata_labels.AddRange (((Parameters)parameters).dataset1.GetAvailableMetadata (null));
				merged_metadata_labels.AddRange (((Parameters)parameters).dataset2.GetAvailableMetadata (null));			
			}
			
			//todo: make unique
			
			return merged_metadata_labels.ToArray ();
		}


	 

		
		//var dataset = {Dataset}
		//union(dataset, dataset_other)
		//dataset
		
		/// <summary>
		/// Merges the datasets given in the parameters.
		/// </summary>
		protected void MergeDatasets ()
		{
			Console.WriteLine ("> Collapsing ids .."); //TODO: add a decent logger
			Parameters casted_parameters = ((Parameters)(GetParameters()));
		
			//Calculate needed capacity
			this.idList.Capacity = casted_parameters.dataset1.Count + casted_parameters.dataset2.Count;
			
			//Add new ids (we need to distinguish them, because we cannot guarantee that they won't repeat)
			foreach (object id in casted_parameters.dataset1.Keys ())
				this.idList.Add (new MyID (1, id));
			foreach (object id in casted_parameters.dataset2.Keys ())
				this.idList.Add (new MyID (2, id));
			
			Console.WriteLine ("> Collapsed capacity is " + this.idList.Capacity);
		}
		
		/// <summary>
		/// Casts the identifier and throws an error if the format is wrong. It accepts nulls
		/// </summary>
		protected MyID CastId (object id)
		{
			MyID casted_id = id as MyID;
			if (casted_id == null && id != null) {
				throw new WIODataLanguageWrongFormatException (); 
			} 
			if (casted_id != null && (casted_id.Item1 != 1 && casted_id.Item1 != 2)) {
				throw new WIODataLanguageException ("Wrong ID format"); //todo: add Validate function
			}
			return casted_id;
		}
		
		/// <summary>
		/// Parameters class for the Dataset
		/// </summary>
		public class Parameters{
			public IDataset dataset1, dataset2;
		}	

		
		//i had to implement it to allow for union of union
		public class MyID: ICloneable{
			public int Item1;
			public object Item2;
			public MyID(int index, object val){
				this.Item1 = index;
				this.Item2 = val;
			}
			public object Clone()
			{
				return new MyID(this.Item1, ((ICloneable)this.Item2).Clone());
			}

			public override string ToString ()
			{
				return "("+Item1.ToString()+","+Item2.ToString()+")";
			}

			public override int GetHashCode ()
			{
				//FIXME: ..
				return new Tuple<int, object>(this.Item1,this.Item2).GetHashCode();
			}

			public override bool Equals (object obj)
			{
				MyID casted = obj as MyID;
				if((Object)casted == null) return false;

				return this.Item1 == casted.Item1 && this.Item2.Equals(casted.Item2); //!! equals..
			}

		}		
	}
		
	/// <summary>
	/// Limit dataset : return the N first element of a dataset. Tries to remove the rest (TryRemoveDataEntry function)
	/// </summary>
	[WIOCallnameAnnotation("Limit")]
	public class LimitDataset : WIODataLanguage.IDataset{
		//te list is for Keys(), and idDict is for keeping (quickly) track of which items we have abilitiy to access
		protected Dictionary<object, bool> idDict = new Dictionary<object, bool>(); //artificial list of ids that are present
		protected List<object> idList = new List<object>(); //artificial list of ids that are present
		
		public LimitDataset(){
		
		}

		//just pass responsibility
		public override object GetMetaByLabel (object id, string name)
		{
			//but check if it is in the dict
			if (!this.idDict.ContainsKey (id))
				throw new WIODataLanguageException ("ID not present in the dataset");
			return ((Parameters)(GetParameters())).dataset.GetMetaByLabel (id, name);
		}

		//pass responsibility to wrapped dataset
		public override Type QueryGetElementType ()
		{
			return ((Parameters)(GetParameters())).dataset.QueryGetElementType();
		}
		
		//just pass responsibility
		public override string[] GetAvailableMetadata (object id = null)
		{ 
			//id can be null
			if (id!=null && !this.idDict.ContainsKey (id))
				throw new WIODataLanguageException ("ID not present in the dataset");
			return ((Parameters)(GetParameters())).dataset.GetAvailableMetadata (id);
		}
		
		//check if possible, and if yes just pass responsibility
		[WIODataLanguage.DatasetTypeAttribute(typeof(object))]     
		public override object GetElement (object id)
		{
			if (!this.idDict.ContainsKey (id))
				throw new WIODataLanguageException ("ID not present in the dataset");
			return ((Parameters)(GetParameters())).dataset.GetElement (id);
		}
		
		public override IEnumerable Keys ()
		{
			foreach (object id in idList)
				yield return ((ICloneable)id).Clone ();
		}
		
		public override void UpdateParameters (object parameters)
		{
			Parameters casted_parameters = parameters as Parameters;
			
			if (casted_parameters == null)
				throw new WIODataLanguageWrongFormatException ("Wrong format of parameters");
			
			this.parameters = parameters;//new Parameters{dataset = casted_parameters.dataset, N = casted_parameters.N, removeItems = casted_parameters.removeItems};
		
			//Reset

			this.idList.Clear();
			this.idDict.Clear();

			this.ParseDataset ();

			//Correct attribute for GetElement
		}
		
		/// <summary>
		/// Tries the remove element entries. Note that it is often time critical point so I tried to optimize it a little bit to be
		/// O(N) in items to remove (amortized)
		/// </summary>
		public override bool TryRemoveElementEntries (object[] ids)
		{
			bool idDictRemove = true;
			
			Dictionary<object, bool> removeDict = new Dictionary<object, bool>();
			foreach(object id in ids) removeDict[id] = true;
			
			int idListRemove = this.idList.RemoveAll(ele => removeDict.ContainsKey(ele)); //O(N) but batch!!
			bool removeDataset = false;
			
			foreach(object id in ids) //is LINQ faster?
				removeDataset = removeDataset && this.idDict.Remove (id);
					
			((Parameters)(GetParameters())).dataset.TryRemoveElementEntries(ids);
			
			
			return idDictRemove && (idListRemove>0) && removeDataset;					
		}
	
		
		protected void ParseDataset ()
		{
			Console.WriteLine ("> Collapsing ids .."); //TODO: add a decent logger
			Parameters casted_parameters = ((Parameters)(GetParameters()));
		
			//Calculate needed capacity
			this.idList.Capacity = (Int32)casted_parameters.N;
			
			//Add new ids (we need to distinguish them, because we cannot guarantee that they won't repeat)
			List<object> idListTmp = new List<object> (); //i need this temporary list to fire TryRemoveElementEntries
//			//otherwise foreach loop could fail
			idListTmp.Capacity = casted_parameters.dataset.Count; 
			foreach (object id in casted_parameters.dataset.Keys ())
				idListTmp.Add (id);

			
			List<object> batchRemove = new List<object>();
			if(((Parameters)(GetParameters())).removeItems) batchRemove.Capacity = idListTmp.Capacity;
			
			//now we can populate idList properly
			int list_size = idListTmp.Count; 
			for (int i=0; i<list_size; ++i) {
				if (i<casted_parameters.N){
					this.idList.Add (idListTmp [i]);
					this.idDict [idListTmp [i]] = true;
				}
				//thats a tricky part - we are trying to remove from dataset bookeeping ids that are getting limited (and won't be NEVER used,
				//because the idea is that Limit takes COMPLETE ownership of the dataset given in parameters)
				else{
					if(((Parameters)(GetParameters())).removeItems) batchRemove.Add(idListTmp[i]);				
				}
			}
			
			if(((Parameters)(GetParameters())).removeItems) ((Parameters)(GetParameters())).dataset.TryRemoveElementEntries(batchRemove.ToArray());
			Console.WriteLine ("> Collapsed capacity is " + this.idList.Count);
		}
		public class Parameters{
			public IDataset dataset;
			public Int64 N = 100;
			public bool removeItems = true;
		}	
	}
	
	/// <summary>
	/// Takes two datasets and removes one entries from another. Notice that dataset doesn't implement 
	/// RemoveElement, it will remove them artificially only.
	/// </summary>
	[WIOCallnameAnnotation("Exclude")]
	public class ExcludeDataset : WIODataLanguage.IDataset{
		//te list is for Keys(), and idDict is for keeping (quickly) track of which items we have abilitiy to access
		protected Dictionary<object, bool> idDict = new Dictionary<object, bool>(); //artificial list of ids that are present
		protected List<object> idList = new List<object>(); //artificial list of ids that are present
		
		public ExcludeDataset(){
			
		}
		
		//just pass responsibility
		public override object GetMetaByLabel (object id, string name)
		{
			//but check if it is in the dict
			if (!this.idDict.ContainsKey (id))
				throw new WIODataLanguageException ("ID not present in the dataset");
			return ((Parameters)(GetParameters())).dataset.GetMetaByLabel (id, name);
		}
		
		//just pass responsibility
		public override string[] GetAvailableMetadata (object id = null)
		{
			//id can be null
			if (id!=null && !this.idDict.ContainsKey (id))
				throw new WIODataLanguageException ("ID not present in the dataset");
			return ((Parameters)(GetParameters())).dataset.GetAvailableMetadata (id);
		}
		//pass responsibility to wrapped dataset
		public override Type QueryGetElementType ()
		{
			return ((Parameters)(GetParameters())).dataset.QueryGetElementType();
		}		
		//check if possible, and if yes just pass responsibility
		[WIODataLanguage.DatasetTypeAttribute(typeof(object))]   
		public override object GetElement (object id)
		{
			if (!this.idDict.ContainsKey (id))
				throw new WIODataLanguageException ("ID not present in the dataset");
			return ((Parameters)(GetParameters())).dataset.GetElement (id);
		}
		
		public override IEnumerable Keys ()
		{
			foreach (object id in idList)
				yield return ((ICloneable)id).Clone ();
		}
		
		public override void UpdateParameters (object parameters)
		{
			Parameters casted_parameters = parameters as Parameters;
			
			if (casted_parameters == null)
				throw new WIODataLanguageWrongFormatException ("Wrong format of parameters");
			
			this.parameters = parameters;// new Parameters{dataset= casted_parameters.dataset, exdataset = casted_parameters.exdataset, removeItems = casted_parameters.removeItems};
			
			//Reset
			this.idList.Clear();
			this.idDict.Clear();

			this.MergeDatasets ();
		}
		
		public override bool TryRemoveElementEntries (object [] ids)
		{
			bool idDictRemove = true;
			
			Dictionary<object, bool> removeDict = new Dictionary<object, bool>();
			foreach(object id in ids) removeDict[id] = true;
			
			int idListRemove = this.idList.RemoveAll(ele => removeDict.ContainsKey(ele)); //O(N) but batch!!
			bool removeDataset = false;
			
			foreach(object id in ids) //is LINQ faster?
				removeDataset = removeDataset && this.idDict.Remove (id);
					
			((Parameters)(GetParameters())).dataset.TryRemoveElementEntries(ids);
			
			
			return idDictRemove && (idListRemove>0) && removeDataset;	
		}
		
		protected void MergeDatasets ()
		{
			Console.WriteLine ("> Collapsing ids .."); //TODO: add a decent logger
			Parameters casted_parameters = ((Parameters)(GetParameters()));
			

			//Add new ids (we need to distinguish them, because we cannot guarantee that they won't repeat)
			int counter = 0;
			List<object> idListTmp = new List<object> (); //i need this temporary list to fire TryRemoveElementEntries
			//otherwise foreach loop could fail
			idListTmp.Capacity = casted_parameters.dataset.Count; 
			foreach (object id in casted_parameters.dataset.Keys ())
				idListTmp.Add (id);

			Dictionary<object, bool> exidDict = new Dictionary<object, bool>();
			foreach(object id in casted_parameters.exdataset.Keys())
				exidDict[id] = true; //mark which are present in the exclude dataset

	
			List<object> removeBatch = new List<object>();
			if(((Parameters)(GetParameters())).removeItems) removeBatch.Capacity = idListTmp.Capacity;
			
			//now we can populate idList properly
			int list_size = idListTmp.Count; 
			for (int i=0; i<list_size; ++i) {
				if (!exidDict.ContainsKey(idListTmp[i])){
					//not in the exclude dataset - so process

					this.idList.Add (idListTmp [i]);
					this.idDict [idListTmp [i]] = true;
				}else{
					++ counter;
					if(((Parameters)(GetParameters())).removeItems) removeBatch.Add (idListTmp[i]);
					//tryremove shouldnt throw error (try should be safe)

				}

			}
			
			if(((Parameters)(GetParameters())).removeItems) ((Parameters)(GetParameters())).dataset.TryRemoveElementEntries(removeBatch.ToArray());
			if(((Parameters)(GetParameters())).removeItems) ((Parameters)(GetParameters())).exdataset.TryRemoveElementEntries(removeBatch.ToArray());
			
			Console.WriteLine ("> Collapsed capacity is " + this.idList.Count + " removed "+counter + " elements");
		}
		public class Parameters{
			public IDataset dataset;
			public IDataset exdataset;
			public bool removeItems = true;
		}	
	}
	
	/// <summary>
	/// Retrieves K N-element chunk from the dataset (note : first chunk is 1 not 0)
	/// </summary>
	[WIOCallnameAnnotation("Chunk")]
	public class ChunkDataset : WIODataLanguage.IDataset{
		//te list is for Keys(), and idDict is for keeping (quickly) track of which items we have abilitiy to access
		protected Dictionary<object, bool> idDict = new Dictionary<object, bool>(); //artificial list of ids that are present
		protected List<object> idList = new List<object>(); //artificial list of ids that are present
		
		public ChunkDataset(){
			
		}
		
		//just pass responsibility
		public override object GetMetaByLabel (object id, string name)
		{
			//but check if it is in the dict
			if (!this.idDict.ContainsKey (id))
				throw new WIODataLanguageException ("ID not present in the dataset");
			return ((Parameters)(GetParameters())).dataset.GetMetaByLabel (id, name);
		}
		
		//just pass responsibility
		public override string[] GetAvailableMetadata (object id = null)
		{
			//id can be null
			if (id!=null && !this.idDict.ContainsKey (id))
				throw new WIODataLanguageException ("ID not present in the dataset");
			return ((Parameters)(GetParameters())).dataset.GetAvailableMetadata (id);
		}
		//pass responsibility to wrapped dataset
		public override Type QueryGetElementType ()
		{
			return ((Parameters)(GetParameters())).dataset.QueryGetElementType();
		}
		//check if possible, and if yes just pass responsibility
		[WIODataLanguage.DatasetTypeAttribute(typeof(object))]   
		public override object GetElement (object id)
		{
			if (!this.idDict.ContainsKey (id))
				throw new WIODataLanguageException ("ID not present in the dataset");
			return ((Parameters)(GetParameters())).dataset.GetElement (id);
		}
		
		public override IEnumerable Keys ()
		{
			foreach (object id in idList)
				yield return ((ICloneable)id).Clone ();
		}
		
		public override void UpdateParameters (object parameters)
		{
			Parameters casted_parameters = parameters as Parameters;
			
			if (casted_parameters == null)
				throw new WIODataLanguageWrongFormatException ("Wrong format of parameters");
			
			this.parameters = parameters;//new Parameters{dataset = casted_parameters.dataset, N = casted_parameters.N, K=casted_parameters.K, removeItems = casted_parameters.removeItems};
			
			//Reset
			this.idList.Clear();
			this.idDict.Clear();

			this.ParseDataset ();
		}
		
		public override bool TryRemoveElementEntries (object [] ids)
		{
			bool idDictRemove = true;
			
			Dictionary<object, bool> removeDict = new Dictionary<object, bool>();
			foreach(object id in ids) removeDict[id] = true;
			
			int idListRemove = this.idList.RemoveAll(ele => removeDict.ContainsKey(ele)); //O(N) but batch!!
			bool removeDataset = false;
			
			foreach(object id in ids) //is LINQ faster?
				removeDataset = removeDataset && this.idDict.Remove (id);
					
			((Parameters)(GetParameters())).dataset.TryRemoveElementEntries(ids);
			
			
			return idDictRemove && (idListRemove>0) && removeDataset;	
		}
		
		protected void ParseDataset ()
		{
			Console.WriteLine ("> Collapsing ids .."); //TODO: add a decent logger
			Parameters casted_parameters = ((Parameters)(GetParameters()));
			
			
			//Add new ids (we need to distinguish them, because we cannot guarantee that they won't repeat)
			List<object> idListTmp = new List<object> (); //i need this temporary list to fire TryRemoveElementEntries
			//otherwise foreach loop could fail
			idListTmp.Capacity = casted_parameters.dataset.Count; 
			foreach (object id in casted_parameters.dataset.Keys ())
				idListTmp.Add (id);
			
			//check if this chunk is possible to retrieve
			if( casted_parameters.N*casted_parameters.K > casted_parameters.dataset.Count) 
				throw new WIODataLanguageException("Too small dataset to retrieve chunk");


			Int64 first_index = (casted_parameters.K-1)*casted_parameters.N; 
			Int64 last_index = (casted_parameters.K)*casted_parameters.N-1;

			
			List<object> removeBatch = new List<object>();
			if(((Parameters)(GetParameters())).removeItems) removeBatch.Capacity = idListTmp.Capacity;
			
			
			//now we can populate idList properly
			int list_size = idListTmp.Count; 
			for (int i=0; i<list_size; ++i) {
				if (i>=first_index && i<=last_index){
					this.idList.Add (idListTmp [i]);
					this.idDict [idListTmp [i]] = true;
				}else{
					if(((Parameters)(GetParameters())).removeItems) removeBatch.Add (idListTmp[i]);
					//tryremove shouldnt throw error (try should be safe)
					
				}
				
			}
			if(((Parameters)(GetParameters())).removeItems)  ((Parameters)(GetParameters())).dataset.TryRemoveElementEntries(removeBatch.ToArray());
			Console.WriteLine ("> Collapsed capacity is " + this.idList.Count);
		}
		public class Parameters{
			public IDataset dataset;
			public Int64 N=10, K=1; //it is Int64 for serializer compability
			public bool removeItems = true; //remove rest of the dataset or just "borrow" these, that are used currently and leave the rest for being used
		}	
	}

	/// <summary>
	/// Filters dataset. It is functional-like, but instead of predicate the parameters is a json list. Ex
	/// ["id1","id2","id3"]
	/// When creating the dataset, it will assume false for all nonpresent ids.
	/// Note : "id1", etc are serializations of ids. Serialization is assumed to be done using popular json library:
	/// Newtonsoft.Json. 
	/// </summary>
	[WIOCallnameAnnotation("Filter")]
	public class FilterDataset : WIODataLanguage.IDataset{
		//te list is for Keys(), and idDict is for keeping (quickly) track of which items we have abilitiy to access
		protected Dictionary<object, bool> idDict = new Dictionary<object, bool>(); //artificial list of ids that are present
		protected List<object> idList = new List<object>(); //artificial list of ids that are present
		
		public FilterDataset(){
			
		}
		
		//just pass responsibility
		public override object GetMetaByLabel (object id, string name)
		{
			//but check if it is in the dict
			if (!this.idDict.ContainsKey (id))
				throw new WIODataLanguageException ("ID not present in the dataset");
			return ((Parameters)(GetParameters())).dataset.GetMetaByLabel (id, name);
		}
		
		//just pass responsibility
		public override string[] GetAvailableMetadata (object id = null)
		{
			//id can be null
			if (id!=null && !this.idDict.ContainsKey (id))
				throw new WIODataLanguageException ("ID not present in the dataset");
			return ((Parameters)(GetParameters())).dataset.GetAvailableMetadata (id);
		}
		//pass responsibility to wrapped dataset
		public override Type QueryGetElementType ()
		{
			return ((Parameters)(GetParameters())).dataset.QueryGetElementType();
		}
		//check if possible, and if yes just pass responsibility
		[WIODataLanguage.DatasetTypeAttribute(typeof(object))]   
		public override object GetElement (object id)
		{
			if (!this.idDict.ContainsKey (id))
				throw new WIODataLanguageException ("ID not present in the dataset");
			return ((Parameters)(GetParameters())).dataset.GetElement (id);
		}
		
		public override IEnumerable Keys ()
		{
			foreach (object id in idList)
				 if(id is ICloneable) yield return ((ICloneable)id).Clone ();
				else yield return id;	
		}
		
		public override void UpdateParameters (object parameters)
		{
			Parameters casted_parameters = parameters as Parameters;
			
			if (casted_parameters == null)
				throw new WIODataLanguageWrongFormatException ("Wrong format of parameters");
			
			this.parameters= parameters;//new Parameters{dataset = casted_parameters.dataset, include_list = casted_parameters.include_list, removeItems = casted_parameters.removeItems};
			
			//Reset
			this.idList.Clear();
			this.idDict.Clear();


			this.ParseDataset ();
		}
		
		public override bool TryRemoveElementEntries (object [] ids)
		{
			bool idDictRemove = true;
			
			Dictionary<object, bool> removeDict = new Dictionary<object, bool>();
			foreach(object id in ids) removeDict[id] = true;
			
			int idListRemove = this.idList.RemoveAll(ele => removeDict.ContainsKey(ele)); //O(N) but batch!!
			bool removeDataset = false;
			
			foreach(object id in ids) //is LINQ faster?
				removeDataset = removeDataset && this.idDict.Remove (id);
					
			((Parameters)(GetParameters())).dataset.TryRemoveElementEntries(ids);
			
			
			return idDictRemove && (idListRemove>0) && removeDataset;	
		}
		
		protected void ParseDataset ()
		{
			Console.WriteLine ("> Collapsing ids .."); //TODO: add a decent logger
			Parameters casted_parameters = ((Parameters)(GetParameters()));

			//It is imporant for deserialization
			Type dataset_id_type = null; object tmp = null;
			//Add new ids (we need to distinguish them, because we cannot guarantee that they won't repeat)
			List<object> idListTmp = new List<object> (); //i need this temporary list to fire TryRemoveElementEntries
			//otherwise foreach loop could fail
			idListTmp.Capacity = casted_parameters.dataset.Count; 
			foreach (object id in casted_parameters.dataset.Keys ()){
				idListTmp.Add (id);
				tmp = id;
			}

			// watch out for empty datasets
			if(tmp != null) dataset_id_type = tmp.GetType();

			// we will store here which ids are to be inlucded

			Dictionary<object, bool> includeIds = new Dictionary<object, bool>();
			try{
				//objective : fillup includeIds
				string[] includeIdsSerialization = Newtonsoft.Json.JsonConvert.DeserializeObject<string[]>(casted_parameters.include_list);

				Console.WriteLine("Ids to filter : " + includeIdsSerialization.Length);

				foreach(string idSerialization in includeIdsSerialization)
					includeIds[Newtonsoft.Json.JsonConvert.DeserializeObject(idSerialization, dataset_id_type)] = true;

			}
			catch(Exception e){
				throw new WIODataLanguageException("Error when converting JSON using NewtonSoft, are you using NewtonSoft for serialization ? "+e.ToString());
			}

			List<object> removeBatch = new List<object>();
			if(((Parameters)(GetParameters())).removeItems) removeBatch.Capacity = idListTmp.Capacity;
			
			//now we can populate idList properly
			int list_size = idListTmp.Count; 
			for (int i=0; i<list_size; ++i) {
				if (includeIds.ContainsKey(idListTmp[i])){ //predicate if to include
					this.idList.Add (idListTmp [i]);
					this.idDict [idListTmp [i]] = true;
				}else{
					//tryremove shouldnt throw error (try should be safe)
					if(((Parameters)(GetParameters())).removeItems) removeBatch.Add (idListTmp[i]);
					
				}
				
			}
			if(((Parameters)(GetParameters())).removeItems) ((Parameters)(GetParameters())).dataset.TryRemoveElementEntries(removeBatch.ToArray());
			
			Console.WriteLine ("> Collapsed capacity is " + this.idList.Count);
		}
		public class Parameters{
			public IDataset dataset;
			public string include_list = "";
			public bool removeItems = true;
		}	
	}	

	/// <summary>
	/// Shuffle the elements of a dataset according to a random-seed (the function is actually deterministic ! 
	/// as so far, we want all dataset expression to return a deterministic result) 
	/// 
	/// Accepts two parameters. 
	/// 	-- dataset object
	/// 	-- random seed
	/// </summary>
	[WIOCallnameAnnotation("Shuffle")]
	public class ShuffleDataset : WIODataLanguage.IDataset{
		//te list is for Keys(), and idDict is for keeping (quickly) track of which items we have abilitiy to access
		protected List<object> idList = new List<object>(); //artificial list of ids that are present
		
		public ShuffleDataset(){
			
		}
		
		//just pass responsibility
		public override object GetMetaByLabel (object id, string name)
		{
			return ((Parameters)(GetParameters())).dataset.GetMetaByLabel (id, name);
		}
		
		//just pass responsibility
		public override string[] GetAvailableMetadata (object id = null)
		{
			return ((Parameters)(GetParameters())).dataset.GetAvailableMetadata (id);
		}



		//check if possible, and if yes just pass responsibility
		[WIODataLanguage.DatasetTypeAttribute(typeof(object))]   
		public override object GetElement (object id)
		{
			return ((Parameters)(GetParameters())).dataset.GetElement (id);
		}
		//pass responsibility to wrapped dataset
		public override Type QueryGetElementType ()
		{
			return ((Parameters)(GetParameters())).dataset.QueryGetElementType();
		}
		public override IEnumerable Keys ()
		{
			foreach (object id in idList)
				 if(id is ICloneable) yield return ((ICloneable)id).Clone ();
				else yield return id;			
		}
		
		public override void UpdateParameters (object parameters)
		{
			Parameters casted_parameters = parameters as Parameters;
			
			if (casted_parameters == null)
				throw new WIODataLanguageWrongFormatException ("Wrong format of parameters");
			
			this.parameters = parameters;// new Parameters{dataset = casted_parameters.dataset, seed = casted_parameters.seed};
			
			//Reset
			this.idList.Clear();

			this.PrepareDatasets ();
		}
		
		public override bool TryRemoveElementEntries (object [] ids)
		{
			bool idDictRemove = true;
			
			Dictionary<object, bool> removeDict = new Dictionary<object, bool>();
			foreach(object id in ids) removeDict[id] = true;
			
			int idListRemove = this.idList.RemoveAll(ele => removeDict.ContainsKey(ele)); //O(N) but batch!!

			((Parameters)(GetParameters())).dataset.TryRemoveElementEntries(ids);
			
			
			return idDictRemove && (idListRemove>0);	
		}
		
		protected void PrepareDatasets ()
		{
			Console.WriteLine ("> Collapsing ids .."); //TODO: add a decent logger
			Parameters casted_parameters = ((Parameters)(GetParameters()));

			//It is imporant for deserialization

			int dataset_size = casted_parameters.dataset.Count;

			//Add new ids (we need to distinguish them, because we cannot guarantee that they won't repeat)
			List<object> idListTmp = new List<object> (); //i need this temporary list to fire TryRemoveElementEntries
			//otherwise foreach loop could fail
			idListTmp.Capacity = dataset_size; 
			idList.Capacity = dataset_size;
			foreach (object id in casted_parameters.dataset.Keys ()){
				idListTmp.Add (id);
			}

			// do the shuffle
			Random r = new Random((int)casted_parameters.seed);
			int[] indexes = Enumerable.Range(0, dataset_size).ToArray();
			indexes = indexes.OrderBy(x => r.Next()).ToArray();


			for(int i=0; i<dataset_size;++i){
				this.idList.Add (idListTmp[indexes[i]]);
			}
			
			Console.WriteLine ("> Collapsed capacity is " + this.idList.Count);
		}
		public class Parameters{
			public IDataset dataset;
			public Int64 seed =0;
		}	
	}	

	/// <summary>
	/// Zip 2 datasets : only keys that have same ids are left. However even if 2 datasets have same ids
	/// it does not guarantee that the underlying element is the same. Therefore GetElement will return a tuple of 
	/// two elements. It implements proper attribute to keep track of Type
	/// </summary>
	[WIOCallnameAnnotation("Zip")]
	public class ZipDataset : WIODataLanguage.IDataset{

		protected Dictionary<object, bool> idDict = new Dictionary<object, bool>(); //artificial list of ids that are present
		protected List<object> idList = new List<object>(); //artificial list of ids that are present
		
		public ZipDataset(){
			
		}

		/// <summary>
		/// This function returns a tuple consisting of 2 objects. Value in tuple is not null if label query succedded.
	    /// It is designed this way, because we cannot guarantee that metadata names are distinct.
		/// datasets
		/// </summary>
		[WIODataLanguage.DatasetTypeAttribute(typeof(Tuple<object, object>))]
		public override object GetMetaByLabel (object id, string name)
		{

			//but check if it is in the dict
			if (!this.idDict.ContainsKey (id))
				throw new WIODataLanguageException ("ID not present in the dataset");

			object meta1 = null, meta2 = null;
			Tuple<object,object> created_tuple = new Tuple<object,object>(meta1, meta2);
			try{
				meta1 = ((Parameters)(GetParameters())).dataset1.GetMetaByLabel (id, name);
			}catch(Exception){
				
			}
			try{
				meta2 = ((Parameters)(GetParameters())).dataset2.GetMetaByLabel (id, name);
			}catch(Exception){
				
			}		

			if(meta1==null && meta2==null) throw new WIODataLanguageException("Label not present in the dataset");

			return created_tuple;
		}


		/// <summary>
		/// Gets the available metadata. It is a merged list of available metadata in dataset1 and dataset2
		/// </summary>
		/// <returns>
		public override string[] GetAvailableMetadata (object id = null)
		{ 
			List<string> merged_metadata_labels = new List<string> ();
				
			merged_metadata_labels.AddRange (((Parameters)parameters).dataset1.GetAvailableMetadata (null));
			merged_metadata_labels.AddRange (((Parameters)parameters).dataset2.GetAvailableMetadata (null));			

			return merged_metadata_labels.ToArray ();
		}
		
		//check if possible, and if yes just pass responsibility
		[WIODataLanguage.DatasetTypeAttribute(typeof(object))]   
		public override object GetElement (object id)
		{
			if (!this.idDict.ContainsKey (id))
				throw new WIODataLanguageException ("ID not present in the dataset");






			return typeof(Tuple<,>).MakeGenericType(new 
			                                        Type[]{
				((Parameters)parameters).dataset1.QueryGetElementType(),
				((Parameters)parameters).dataset2.QueryGetElementType()
			}).InvokeMember(
				null,
				BindingFlags.CreateInstance,
				null,
				null, 
				new object[]{
				(((Parameters)(GetParameters())).dataset1.GetElement (id)),  
				((Parameters)(GetParameters())).dataset2.GetElement (id)
			}
			);

		}

		public override Type QueryGetElementType ()
		{
			//reflection magic.
			return (typeof(Tuple<,>).MakeGenericType(new Type[]{((Parameters)parameters).dataset1.QueryGetElementType(),
				((Parameters)parameters).dataset2.QueryGetElementType()}));
		}

		public override IEnumerable Keys ()
		{
			foreach (object id in idList)
				yield return ((ICloneable)id).Clone ();
		}
		
		public override void UpdateParameters (object parameters)
		{
			Parameters casted_parameters = parameters as Parameters;
			
			if (casted_parameters == null)
				throw new WIODataLanguageWrongFormatException ("Wrong format of parameters");

			this.parameters= parameters;//new Parameters{dataset1 = casted_parameters.dataset1, dataset2 = casted_parameters.dataset2,
				//removeItems = casted_parameters.removeItems};

			//Reset
			this.idDict.Clear();
			this.idList.Clear();
			
			this.MergeDatasets ();
		}
		
		public override bool TryRemoveElementEntries (object [] ids)
		{
			bool idDictRemove = true;
			
			Dictionary<object, bool> removeDict = new Dictionary<object, bool>();
			foreach(object id in ids) removeDict[id] = true;
			
			int idListRemove = this.idList.RemoveAll(ele => removeDict.ContainsKey(ele)); //O(N) but batch!!
			bool removeDataset = false;
			
			foreach(object id in ids) //is LINQ faster?
				removeDataset = removeDataset && this.idDict.Remove (id);
					
			((Parameters)(GetParameters())).dataset1.TryRemoveElementEntries(ids);
			((Parameters)(GetParameters())).dataset2.TryRemoveElementEntries(ids);
			
			return idDictRemove && (idListRemove>0) && removeDataset;	
		}
		
		
		protected void MergeDatasets ()
		{
			Console.WriteLine ("> Collapsing ids .."); //TODO: add a decent logger
			Parameters casted_parameters = ((Parameters)(GetParameters()));

			//Add new ids (we need to distinguish them, because we cannot guarantee that they won't repeat)
			List<object> idListTmp1 = new List<object> (); //i need this temporary list to fire TryRemoveElementEntries
			//otherwise foreach loop could fail
			idListTmp1.Capacity = casted_parameters.dataset1.Count; 
			foreach (object id in casted_parameters.dataset1.Keys ())
				idListTmp1.Add (id);

			List<object> idListTmp2 = new List<object> (); //i need this temporary list to fire TryRemoveElementEntries
			//otherwise foreach loop could fail
			idListTmp2.Capacity = casted_parameters.dataset2.Count; 
			foreach (object id in casted_parameters.dataset2.Keys ())
				idListTmp2.Add (id);


			Dictionary<object, bool> idDict2 = new Dictionary<object, bool>();
			foreach (object id in casted_parameters.dataset2.Keys ())
					idDict2[id] = true;


			List<object> removeBatch1 = new List<object>();
			List<object> removeBatch2 = new List<object>();
			if(((Parameters)(GetParameters())).removeItems) removeBatch1.Capacity = idListTmp1.Capacity;
			if(((Parameters)(GetParameters())).removeItems) removeBatch2.Capacity = idListTmp2.Capacity;
			
			//now we can populate idList properly
			int list_size = idListTmp1.Count; 
			for (int i=0; i<list_size; ++i) {
				if (idDict2.ContainsKey(idListTmp1[i])){
					this.idDict[idListTmp1[i]] = true;
					this.idList.Add (idListTmp1[i]);
				}
				else{
					if(((Parameters)(GetParameters())).removeItems) removeBatch1.Add(idListTmp1[i]);
					
				}
			}
			if(((Parameters)(GetParameters())).removeItems) casted_parameters.dataset1.TryRemoveElementEntries(removeBatch1.ToArray());
		    list_size = idListTmp2.Count; 
			for (int i=0; i<list_size; ++i) {
			 	if(!this.idDict.ContainsKey(idListTmp2[i])) {
					if(((Parameters)(GetParameters())).removeItems) removeBatch1.Add(idListTmp2[i]);
					
				}
			}
			if(((Parameters)(GetParameters())).removeItems) casted_parameters.dataset2.TryRemoveElementEntries(removeBatch2.ToArray());
			//remove the rest
			
			Console.WriteLine ("> Collapsed capacity is " + this.idList.Count);
		}
		public class Parameters{
			public IDataset dataset1;
			public IDataset dataset2;
			public bool removeItems = true;
		}
	
	}

	/// <summary>
	/// Returns dataset consisting of all possible pairs of ordered elements (cartesian product), Order matters
	/// </summary>
	[WIOCallnameAnnotation("Product")]
	public class ProductDataset: WIODataLanguage.IDataset{
		//it is imporant for ID to hashable and serailizable easily
		//I have picked tuple<int, object> for ID because there no guarantees for distinct IDs (especially if we are making an operation with a simple database)

		
		protected List<MyID> idList = new List<MyID>();
		protected Dictionary<MyID, bool>  idDict = new Dictionary<MyID, bool>();

		//that's all we can say, it is either of two types
		[WIODataLanguage.DatasetTypeAttribute(typeof(object))]     
		public override object GetElement (object id)
		{
			MyID casted_id = id as MyID;
			
			if (casted_id == null) 
				throw new WIODataLanguageWrongFormatException (); 

			if(this.idDict.ContainsKey(casted_id)) 
				throw new WIODataLanguageException("ID not present in the dataset");

			//this line creates correctly typed tuple, well.. if QueryGetElement fails it returns anyway object so it is still fine
			return typeof(Tuple<,>).MakeGenericType(new 
			            Type[]{
								((Parameters)parameters).dataset1.QueryGetElementType(),
								((Parameters)parameters).dataset2.QueryGetElementType()
						}).InvokeMember(
					null,
					BindingFlags.CreateInstance,
					null,
					null, 
					new object[]{
						(((Parameters)(GetParameters())).dataset1.GetElement (casted_id.Item1)),  
				 		((Parameters)(GetParameters())).dataset2.GetElement (casted_id.Item2)
				 	}
				 );
		}
		
		
		public override IEnumerable Keys ()
		{
			//note : it cannot be foreach because Clone modifies enumeration.. idk how to solve it
			int idListSize = this.idList.Count; 
			for (int i=0; i< idListSize ;++i)
				yield return idList[i].Clone();
		}
		public override void UpdateParameters (object parameters)
		{
			Parameters casted_parameters = parameters as Parameters;
			if(casted_parameters == null) throw new WIODataLanguageException("Wrong parameters format");

			this.parameters = parameters;// new Parameters{dataset1 = casted_parameters.dataset1, dataset2 = casted_parameters.dataset2}; //to be on the safe side, btw : 
			//because for caching we need highly standarized datasets, we can for instance 
			//order dataset1 and dataset2 by calling name (attribute)

			//Reset
			this.idDict.Clear();
			this.idList.Clear();

			//parameters should be a struct to be copyable easily but we need inheritance from time to time
			this.MergeDatasets();
		}
		
		
		[WIODataLanguage.DatasetTypeAttribute(typeof(Tuple<object, object>))]
		public override object GetMetaByLabel (object id, string name)
		{
			MyID casted_id = id as MyID;
			
			if (casted_id == null) 
				throw new WIODataLanguageWrongFormatException (); 

			//but check if it is in the dict
			if (!this.idDict.ContainsKey (casted_id))
				throw new WIODataLanguageException ("ID not present in the dataset");
			
			object meta1 = null, meta2 = null;
			Tuple<object,object> created_tuple = new Tuple<object,object>(meta1, meta2);
			try{
				meta1 = ((Parameters)(GetParameters())).dataset1.GetMetaByLabel (id, name);
			}catch(Exception){
				
			}
			try{
				meta2 = ((Parameters)(GetParameters())).dataset2.GetMetaByLabel (id, name);
			}catch(Exception){
				
			}		
			
			if(meta1==null && meta2==null) throw new WIODataLanguageException("Label not present in the dataset");
			
			return created_tuple;
		}


		public override Type QueryGetElementType ()
		{
			//reflection magic.
			return (typeof(Tuple<,>).MakeGenericType(new Type[]{((Parameters)parameters).dataset1.QueryGetElementType(),
				((Parameters)parameters).dataset2.QueryGetElementType()}));
		}
		
		
		/// <summary>
		/// Gets available metadata (idea is that not every object has to have all the metadata present)
		/// </summary>
		/// <returns>
		/// The available metadata in string[] (simplicity for parser)
		/// </returns>
		/// <summary>
		/// Gets the available metadata. It is a merged list of available metadata in dataset1 and dataset2
		/// </summary>
		/// <returns>
		public override string[] GetAvailableMetadata (object id = null)
		{ 
			List<string> merged_metadata_labels = new List<string> ();
			
			merged_metadata_labels.AddRange (((Parameters)parameters).dataset1.GetAvailableMetadata (null));
			merged_metadata_labels.AddRange (((Parameters)parameters).dataset2.GetAvailableMetadata (null));			
			
			return merged_metadata_labels.ToArray ();
		}
		
		
		
		
		
		//var dataset = {Dataset}
		//union(dataset, dataset_other)
		//dataset
		
		/// <summary>
		/// Merges the datasets given in the parameters.
		/// </summary>
		protected void MergeDatasets ()
		{
			Console.WriteLine ("> Collapsing ids .."); //TODO: add a decent logger
			Parameters casted_parameters = ((Parameters)(GetParameters()));
			
			//Calculate needed capacity
			this.idList.Capacity = casted_parameters.dataset1.Count + casted_parameters.dataset2.Count;
			
			//Add new ids (we need to distinguish them, because we cannot guarantee that they won't repeat)
			foreach (object id_1 in casted_parameters.dataset1.Keys ()){
				foreach (object id_2 in casted_parameters.dataset2.Keys ()){
						MyID created_id = new MyID (id_1,id_2);
						this.idList.Add (created_id);
						this.idDict[created_id] = true;
				}
			}
				
		
			
			Console.WriteLine ("> Collapsed capacity is " + this.idList.Capacity);
		}
		
		/// <summary>
		/// Casts the identifier and throws an error if the format is wrong. It accepts nulls
		/// </summary>
		protected MyID CastId (object id)
		{
			MyID casted_id = id as MyID;
			if (casted_id == null && id != null) {
				throw new WIODataLanguageWrongFormatException (); 
			} 

			return casted_id;
		}
		
		/// <summary>
		/// Parameters class for the Dataset
		/// </summary>
		public class Parameters{
			public IDataset dataset1, dataset2;
		}	
		
		
		//i had to implement it to allow for union of union
		public class MyID: ICloneable{
			public object Item1;
			public object Item2;
			public MyID(object item1, object item2){
				this.Item1 = item1;
				this.Item2 = item2;
			}
			public object Clone()
			{
				return new MyID(((ICloneable)this.Item1).Clone(), ((ICloneable)this.Item2).Clone());
			}
			
			public override string ToString ()
			{
				return "("+Item1.ToString()+","+Item2.ToString()+")";
			}
			
			public override int GetHashCode ()
			{
				//FIXME: ..
				return new Tuple<object, object>(this.Item1,this.Item2).GetHashCode();
			}
			
			public override bool Equals (object obj)
			{
				MyID casted = obj as MyID;
				if((Object)casted == null) return false;
				
				return this.Item1.Equals (casted.Item1) && this.Item2.Equals(casted.Item2);
			}
			
		}		
	}




}

