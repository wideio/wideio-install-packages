using System;
using System.Diagnostics;
using System.Collections;
using System.IO;
using System.Text;
using System.Collections.Generic;
using System.ServiceModel.Web;           // for DataContractJsonSerializer. Need reference too.
using System.Runtime.Serialization.Json; // for DataContractJsonSerializer
using System.Text.RegularExpressions;
using System.Reflection;
using System.Linq;
using System.Drawing;
using WIODataLanguage;

using Newtonsoft.Json;

using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;

namespace WIODatasetLanguage
{
	[WIOCallnameAnnotation("Bang")]
	public class BangDataset : WIODataLanguage.IDataset{
		public override object GetElement (object id)
		{
			return null;
		}
		public override IEnumerable Keys ()
		{
			yield return "bang";
		}
		public override void UpdateParameters (object p)
		{
			return;
		}
		
		public new class Parameters{
		}
	}


	//not to be called//
	public class WrapFunctionDataset : WIODataLanguage.IDataset{
	
		public IDataset wrapped_dataset;

		public virtual void CreateDataset(){
			((Parameters)(GetParameters())).source_dataset = new BangDataset();
		}


		public override void UpdateParameters (object p)
		{
			Parameters casted_parameters = p as Parameters;
			if(casted_parameters==null) throw new WIODataLanguageException("Wrong parameters format");

			this.parameters = p;//new Parameters{source_dataset = casted_parameters.source_dataset};

			this.CreateDataset();
		}

		public override IEnumerable Keys ()
		{
			return this.wrapped_dataset.Keys();
		}

		public override object GetElement (object id)
		{
			return this.wrapped_dataset.GetElement(id);
		} 


		public override Type QueryGetElementType ()
		{
			return this.wrapped_dataset.QueryGetElementType();
		}

		public override string[] GetAvailableMetadata (object id = null)
		{
			return this.wrapped_dataset.GetAvailableMetadata(id);
		}

		public override object GetMetaByLabel (object id, string name)
		{
			return this.wrapped_dataset.GetMetaByLabel(id, name);
		}

		public override bool TryRemoveElementEntries (object[] ids)
		{
			return this.wrapped_dataset.TryRemoveElementEntries(ids);
		}


		public class Parameters{
			public IDataset source_dataset = null;
		}
	
	}

	
	/// <summary>
	/// Image filter util class using OpenCV to perform operations (EmguCV wrapper)
	/// </summary>
	public static class ImageFilter{
	
		/// <summary>
		/// The brightness filter. Parameter is double number indicating scale (like 2x brighter)
		/// </summary>
		public static string FILTER_BRIGHTNESS = "FILTER_BRIGHTNESS";
		/// <summary>
		/// Similar to brightness filter
		/// </summary>
		public static string FILTER_CONTRAST = "FILTER_CONTRAST";
		/// <summary>
		/// Parameters are how much to "take" from each side in pixel [left,right,top,bottom]
		/// </summary>
		public static string FILTER_CROP = "FILTER_CROP";
		/// <summary>
		/// Parameter is scale, or 2 parameters [target_width, target_hegiht]
		/// </summary>
		public static string FILTER_RESIZE = "FILTER_RESIZE";

		public static string FILTER_IDENTITY = "FILTER_IDENTITY";
		public static string FILTER_HFLIP = "FILTER_HFLIP";
		public static string FILTER_VFLIP = "FILTER_VFLIP";	
		/// <summary>
		/// Parameters is rotation in degrees, can supply 3 additional parameters that will change rotation background (int RGB format)
		/// </summary>
		public static string FILTER_ROTATION = "FILTER_ROTATION";
		
		
		public static System.Drawing.Image applyFilter(System.Drawing.Image source, string filter, object[] parameters){
			System.Drawing.Bitmap bmpImage = new System.Drawing.Bitmap((System.Drawing.Image)source);
			Image<Bgr, Byte> img = new Image<Bgr,Byte>(bmpImage);
			
			
			
			if(filter == ImageFilter.FILTER_BRIGHTNESS){
				img = img.Mul ((double)parameters[0]);	
			}else if(filter == ImageFilter.FILTER_CONTRAST){
				img = img.AddWeighted(img, 0.0, (double)parameters[0], 0.0);
			}else if(filter == ImageFilter.FILTER_RESIZE){
				if(parameters.Length == 2)
					img = img.Resize((int)parameters[0],(int)parameters[1], INTER.CV_INTER_CUBIC);
				else if(parameters.Length == 1)
					img = img.Resize((double)parameters[0], INTER.CV_INTER_CUBIC);
				else 
					throw new WIODataLanguageException("Wrong filter parameters");
			}else if(filter == ImageFilter.FILTER_CROP){
				int dx_left =  (int) (double)parameters[0], dx_right =  (int)(double)parameters[1],
				dy_top =   (int)(double)parameters[2], dy_bot =   (int)(double)parameters[3];
				Image<Bgr,byte> img_to_paste = new Image<Bgr, byte>
					(img.Width - (dx_left+dx_right), 
					 img.Height -(dy_top+dy_bot)
					);
				
				img.ROI = new Rectangle(dx_left,
				                        dy_top,
				                        img.Width - (dx_left+dx_right), 
				                        img.Height -(dy_top+dy_bot)
				                        );
				
				CvInvoke.cvCopy(img, img_to_paste, IntPtr.Zero);
				
				img = img_to_paste;
				
			}else if(filter == ImageFilter.FILTER_HFLIP){
				img = img.Flip(FLIP.HORIZONTAL);
			}else if(filter == ImageFilter.FILTER_VFLIP){
				img = img.Flip (FLIP.VERTICAL);
			}else if(filter == ImageFilter.FILTER_ROTATION){
				Bgr bgcolor = new Bgr(0,0,0);
				if(parameters.Length == 4){
					bgcolor.Red = ((double)parameters[1]);
					bgcolor.Blue = ((double)parameters[2]);
					bgcolor.Green = ((double)parameters[3]);
				}
				img = img.Rotate((double)parameters[0], bgcolor);
			}
			
			
			return (System.Drawing.Image)img.ToBitmap();
		}		
		
		/// <summary>
		/// Applies the filter.
		/// </summary>
		/// <returns>
		/// The filter.
		/// </returns>
		/// <param name='source'>
		/// Source.
		/// </param>
		/// <param name='filter'>
		/// Filter.
		/// </param>
		/// <param name='filter_params'>
		/// Filter_params - json array of filters
		/// </param>
		public static System.Drawing.Image applyFilter(System.Drawing.Image source, string filter_cmd){		
			string [] split = filter_cmd.Split(';');			
			object[] parameters = Newtonsoft.Json.JsonConvert.DeserializeObject<object[]>(split[1]);		
			return applyFilter(source, split[0], parameters);
		}
	
	}
	

	[WIOCallnameAnnotation("ImgFilter")]
	public class ImageFilterDataset : WIODataLanguage.IDataset{
		IDataset wrapped_dataset;
		

		
		public override void UpdateParameters (object p)
		{
			Parameters casted_parameters = p as Parameters;
			if(casted_parameters==null) throw new WIODataLanguageException("Wrong parameters format");

			this.parameters= p;//new Parameters{dataset = casted_parameters.dataset, filter = casted_parameters.filter, filter_params = casted_parameters.filter_params};
			
			this.wrapped_dataset = ((Parameters) (GetParameters())).dataset;
		}

		public override IEnumerable Keys ()
		{
			return this.wrapped_dataset.Keys();
		}


		public override object GetElement (object id)
		{
			return ImageFilter.applyFilter(
				(System.Drawing.Image)this.wrapped_dataset.GetElement(id), 
				((Parameters)(GetParameters())).filter, 
				((Parameters)(GetParameters())).filter_params);
		}

		public override Type QueryGetElementType ()
		{
			return this.wrapped_dataset.QueryGetElementType();
		}

		public override string[] GetAvailableMetadata (object id = null)
		{
			return this.wrapped_dataset.GetAvailableMetadata(id);
		}

		public override object GetMetaByLabel (object id, string name)
		{
			return this.wrapped_dataset.GetMetaByLabel(id, name);
		}

		public override bool TryRemoveElementEntries (object[] ids)
		{
			return this.wrapped_dataset.TryRemoveElementEntries(ids);
		}


		public class Parameters{
			public IDataset dataset = null;
			public string filter = ImageFilter.FILTER_IDENTITY;
			/*These are parameters for chosen filter.
			 * 
			 * Possible filters and parameters:
			 * 
			 * Brightness
			 * 	[multipier]
			 * Contrast
			 *  [multipier]
			 * Crop
			 *  [dx_left, dx_right, dy_top, dy_bottom]
			 * HFlip
			 *  []
			 * 
			 * 
			 * 
			 */
			
			public object[] filter_params = null; 
		}	
	}
	
	
	[WIOCallnameAnnotation("ImgFilterStar")]
	public class ImageFilterStarDataset : WIODataLanguage.IDataset{
		IDataset wrapped_dataset;
		Random rnd;
		

		//string - modification
		public List<MyID> ids = new List<MyID>(); 
		public Dictionary<MyID, bool> idsDict = new Dictionary<MyID, bool>();
		public override void UpdateParameters (object p)
		{
			
			
			Parameters casted_parameters = p as Parameters;
			if(casted_parameters==null) throw new WIODataLanguageException("Wrong parameters format");

			this.parameters = p;//new Parameters{dataset = casted_parameters.dataset, filter = casted_parameters.filter, filter_params = casted_parameters.filter_params};
			
			this.rnd = new Random(casted_parameters.seed);
			
			this.wrapped_dataset = ((Parameters) (GetParameters())).dataset;
			
			this.ids.Capacity = this.wrapped_dataset.Count*casted_parameters.count;
			
			foreach(object id in casted_parameters.dataset.Keys()){
				//it works this way that it iterates through parameters , where each pair is "consolidated" into one from this range
				
				
				//repeat process
				for(int i=0;i<casted_parameters.count;++i){
					MyID current_id;
					
					
					object[] created_params = new object[casted_parameters.filter_params.Length/2];
					
					for(int j=0;j<created_params.Length;++j)
						created_params[j] = (object)(
							rnd.NextDouble()*((double)casted_parameters.filter_params[2*j+1]-(double)casted_parameters.filter_params[2*j])+(double)casted_parameters.filter_params[2*j]
							);
					
					current_id = new MyID(
						casted_parameters.filter+";"+JsonConvert.SerializeObject(created_params),
						id
					);
			
					this.ids.Add(current_id);
					this.idsDict[current_id] = true;
				}
				
				
			
			}
						
		}

		public override IEnumerable Keys ()
		{
			return ids.AsEnumerable();
		}


		public override object GetElement (object id)
		{
			MyID casted_id = (MyID)id;
			if(casted_id == null) throw new WIODataLanguageException("Wrong ID format");
			
			
			if(!idsDict.ContainsKey(casted_id)) 
				throw new WIODataLanguageException("ID not present in the dictionary");
			
							
			
			return ImageFilter.applyFilter((System.Drawing.Image)this.wrapped_dataset.GetElement(casted_id.Item2), casted_id.Item1);
		}

		public override Type QueryGetElementType ()
		{
			return this.wrapped_dataset.QueryGetElementType();
		}

		public override string[] GetAvailableMetadata (object id = null)
		{
			MyID casted = (MyID)id;
			return this.wrapped_dataset.GetAvailableMetadata(casted.Item2);
		}

		public override object GetMetaByLabel (object id, string name)
		{
			MyID casted = (MyID)id;
			return this.wrapped_dataset.GetMetaByLabel(casted.Item2, name);
		}

		public override bool TryRemoveElementEntries (object[] ids)
		{
			object[] ids_stripped = ids.Select (x => ((MyID)x).Item2).ToArray();
			return this.wrapped_dataset.TryRemoveElementEntries(ids_stripped);
		}


		public class MyID: ICloneable{
			public string Item1;
			public object Item2;
			public MyID(string item1, object item2){
				this.Item1 = item1;
				this.Item2 = item2;
			}
			public object Clone()
			{
				return new MyID((string)Item1.Clone (), ((ICloneable)this.Item2).Clone());
			}
			
			public override string ToString ()
			{
				return "("+Item1.ToString()+","+Item2.ToString()+")";
			}
			
			public override int GetHashCode ()
			{
				//FIXME: ..
				return new Tuple<string, object>(this.Item1,this.Item2).GetHashCode();
			}
			
			public override bool Equals (object obj)
			{
				MyID casted = obj as MyID;
				if((Object)casted == null) return false;
				
				return this.Item1.Equals (casted.Item1) && this.Item2.Equals(casted.Item2);
			}
			
		}		
		
		public class Parameters{
			public IDataset dataset = null;
			public int seed = 1;
			public int count = 10; //how many to generate from each single one
			public string filter = ImageFilter.FILTER_IDENTITY;
			/*These are parameters for chosen filter.
			 * 
			 * Possible filters and parameters:
			 * 
			 * Brightness
			 * 	[multipier_min, multiplier_max]
			 * Contrast
			 *  [multipier_min, multiplier_max]
			 * Crop
			 *  [dx_left, dx_right, dy_top, dy_bottom]
			 * HFlip
			 *  []
			 * 
			 * 
			 * 
			 */
			
			public object[] filter_params = null; 
		}	
	}
	
	[WIOCallnameAnnotation("TrainingDataset")]
	public class TrainingDataset : WIODatasetLanguage.WrapFunctionDataset{
	

		public override void CreateDataset ()
		{
			Parameters casted_parameters = (Parameters)(GetParameters());
			ShuffleDataset sh = new ShuffleDataset();
			sh.UpdateParameters(new ShuffleDataset.Parameters{dataset = casted_parameters.source_dataset, seed = casted_parameters.seed, 
			
			});
			


			this.wrapped_dataset = new LimitDataset();
			this.wrapped_dataset.UpdateParameters(new LimitDataset.Parameters{removeItems = casted_parameters.removeItems, dataset = sh, N = (int)(casted_parameters.training_percentage*sh.Count)});
		}

		public override void UpdateParameters (object p)
		{
			base.UpdateParameters(p);
			Parameters casted_parameters = p as Parameters;
			if(casted_parameters==null) throw new WIODataLanguageException("Wrong parameters format");
			
			this.parameters = p;//new Parameters{source_dataset = casted_parameters.source_dataset, seed = casted_parameters.seed, 
				// removeItems = casted_parameters.removeItems,
				//training_percentage = casted_parameters.training_percentage};
			
			this.CreateDataset();
		}


		public class Parameters : WrapFunctionDataset.Parameters{
			public Int64 seed = 2;
			public double training_percentage = 0.75;
			public bool removeItems = false;
		}

	}

	[WIOCallnameAnnotation("TestingDataset")]
	public class TestingDataset : WIODatasetLanguage.WrapFunctionDataset{
		
		
		public override void CreateDataset ()
		{
			Parameters casted_parameters = (Parameters)(GetParameters());
			ShuffleDataset sh = new ShuffleDataset();
			sh.UpdateParameters(new ShuffleDataset.Parameters{dataset = casted_parameters.source_dataset, seed = casted_parameters.seed});
			
			this.wrapped_dataset = new LimitDataset();
			this.wrapped_dataset.UpdateParameters(new LimitDataset.Parameters{removeItems = casted_parameters.removeItems, dataset = sh, N = (int)(casted_parameters.test_percentage*sh.Count)});
		}
		
		public override void UpdateParameters (object p)
		{
			base.UpdateParameters(p);
			Parameters casted_parameters = p as Parameters;
			if(casted_parameters==null) throw new WIODataLanguageException("Wrong parameters format");
			
			this.parameters= p;//new Parameters{source_dataset = casted_parameters.source_dataset, seed = casted_parameters.seed, test_percentage = casted_parameters.test_percentage,
				 //removeItems = casted_parameters.removeItems};
			
			this.CreateDataset();
		}
		
		
		public class Parameters : WrapFunctionDataset.Parameters{
			public Int64 seed = 2;
			public double test_percentage = 0.25;
			public bool removeItems = false;
		}
		
	}


	[WIOCallnameAnnotation("Whole")]
	public class WholeDataset : WIODataLanguage.IDataset{



		public override object GetMetaByLabel (object id, string name)
		{
			List<object> meta_datas = new List<object>();
			meta_datas.Capacity = ((Parameters)(GetParameters())).dataset.Count;
			bool success = false;
			foreach(var queried_id in ((Parameters)(GetParameters())).dataset.Keys()){
				object meta = null;
				try{
				   meta = (((Parameters)(GetParameters())).dataset.GetMetaByLabel(queried_id,name));
					success = true;
				}
				catch(Exception){
				
				}
				meta_datas.Add(meta);
			}
			if(!success) throw new WIODataLanguageException("Metadata not present in the dataset");
			return meta_datas;
		}
		
		//pass responsibility to wrapped dataset
		public override Type QueryGetElementType ()
		{
			return typeof(List<>).MakeGenericType(((Parameters)(GetParameters())).dataset.QueryGetElementType());
		}


		public override string[] GetAvailableMetadata (object id = null)
		{ 
			return ((Parameters)(GetParameters())).dataset.GetAvailableMetadata(null); //always null because we ask for metadata of all elements
		}
	
		[WIODataLanguage.DatasetTypeAttribute(typeof(object))]     
		public override object GetElement (object id)
		{
			string casted_id = id as String;
			if(casted_id == null) throw new WIODataLanguageWrongFormatException();
			if(casted_id != "whole") throw new WIODataLanguageException("ID not in the dataset");

			//refect list type
			var listType = typeof(List<>);
			var constructedListType = listType.MakeGenericType(((Parameters)(GetParameters())).dataset.QueryGetElementType());


			//create ienumerable
			List<object> elements = new List<object>();
			foreach(var queried_id in ((Parameters)(GetParameters())).dataset.Keys()) elements.Add(((Parameters)(GetParameters())).dataset.GetElement(queried_id));

			//create list
			var retList = Activator.CreateInstance(constructedListType);

			Console.WriteLine(retList.GetType().Name);

			foreach(var x in elements){
				retList.GetType().GetMethod ("Add").Invoke(retList, new object[]{x});
			}

			return retList;
		}
		
		public override IEnumerable Keys ()
		{
			yield return "whole";
		}
		
		public override void UpdateParameters (object parameters)
		{
			Parameters casted_parameters = parameters as Parameters;
			
			if (casted_parameters == null)
				throw new WIODataLanguageWrongFormatException ("Wrong format of parameters");
			
			this.parameters = parameters;//new Parameters{dataset = casted_parameters.dataset};
		}

		

		public class Parameters{
			public IDataset dataset;
		}	
	
	}



}

