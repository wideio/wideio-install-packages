using System;
using System.Diagnostics;
using System.Collections;
using System.IO;
using System.Text;
using System.Collections.Generic;
using System.ServiceModel.Web;           // for DataContractJsonSerializer. Need reference too.
using System.Runtime.Serialization.Json; // for DataContractJsonSerializer
using System.Text.RegularExpressions;
using System.Reflection;
using System.Linq;
using WIODataLanguage;

using Newtonsoft.Json;

using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Bson.Serialization;
using MongoDB.Driver.Builders;


namespace WIODatasetLanguage
{
	/// <summary>
	/// Creates dataset using a collection in mongo
	/// </summary>
	[WIOCallnameAnnotation("Mongo")]
	public class MongoDataset : WIODataLanguage.IDataset{
		//Note: I have decided to store BsonElement and returned as they are, because that might be more informative.
		//I also assume that the data is this row, and there is no metadata therefore.

		/* Mongo classes, putting them here is better than having some global location for them, because we might have many mongos datasets*/
		MongoClient client = new MongoClient(); //every dataset will establish its own connection
		MongoServer server;
		MongoDatabase db;


		long dataSize = 0;

		public MongoDataset(){
			this.server = client.GetServer(); //now it works locally TODO: check where to set external database
		}

		/// <summary>
		/// Gets the element. Note that it is impossible in many cases to load the dataset into memory, so it will most likely
		/// do a query on the dataset
		/// </summary>
		/// <returns>
		[WIODataLanguage.DatasetTypeAttribute(typeof(BsonElement))]     
		public override object GetElement (object id)
		{

			if(!(id is BsonValue)) throw new WIODataLanguageWrongFormatException();

			var rows =
				db.GetCollection<BsonDocument>(((Parameters)(GetParameters())).collection);



			var query = new QueryDocument("_id", (BsonValue)id);

			foreach(BsonDocument bd in rows.Find (query)){
				return bd;
			}

			throw new WIODataLanguageException("ID not present in the dataset");

		}

		public override IEnumerable Keys ()
		{
			Parameters casted_parameters = parameters as Parameters;
			
			if (casted_parameters == null)
				throw new WIODataLanguageWrongFormatException ("Wrong format of parameters");

			var rows =
				db.GetCollection<BsonDocument>(casted_parameters.collection);
			foreach(BsonDocument el in rows.Find (new QueryDocument())){
				yield return el.GetValue("_id").Clone();
			}
		}
		
		public override void UpdateParameters (object parameters)
		{
			Parameters casted_parameters = parameters as Parameters;
			
			if (casted_parameters == null)
				throw new WIODataLanguageWrongFormatException ("Wrong format of parameters");
			
			this.parameters = parameters;//new Parameters{db = casted_parameters.db, collection = casted_parameters.collection};

			//Reset
			this.dataSize = 0;
			this.ReadRows ();
		}

		protected void ReadRows ()
		{
			Parameters casted_parameters = GetParameters() as Parameters; //already checked in UpdateParameters that (GetParameters()) is Parameters
		
			this.db = server.GetDatabase(casted_parameters.db);
			var rows =
				db.GetCollection<BsonDocument>(casted_parameters.collection);

			this.dataSize = rows.Find (new QueryDocument()).Count();


			Console.WriteLine("> Data size = "+this.dataSize);

		}
		public class Parameters{
			public string db, collection;
		}	
	}
}

