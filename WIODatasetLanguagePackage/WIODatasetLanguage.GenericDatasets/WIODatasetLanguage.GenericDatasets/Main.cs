using System;
using System.Diagnostics;
using NUnit.Framework;
using Newtonsoft.Json;


using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Bson.Serialization;
using MongoDB.Driver.Builders;

using System.Threading;

using Gtk;

using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;

using System.Collections.Generic;
using System.Linq;
//Problem: Massive dataset scenario when loading like this limit(MassiveDataset(<params>), 100) - very inefficient. Will caching solve it?
//Note : Heavy dependence on Newtonsoft.Json (MIT license)
//NUnit, one change
//Problem: with reflection on GetElement that's all we can say, it is either of two types


/*TODO: Write documentation on writing datasets. Imporant points are
 * 
    -- generally : ID has to be ultra simple
	-- cloneable ID
	-- ID should override == operator, very important. It is easy to compare references in C# by mistake (for instance if you ID
	-- is a tuple than you have implement your own tuple with overloaded == operator, because inheritance is forbidden)
	-- tryremoveelemententry -- explain mechanism
	-- no inheritance on ID AT ALL! Or it will break 
	example:
		//good class
		public class MyID: ICloneable{
			public int Item1;
			public object Item2;
			public MyID(int index, object val){
				this.Item1 = index;
				this.Item2 = val;
			}
			public object Clone()
			{
				return new MyID(this.Item1, ((ICloneable)this.Item2).Clone());
			}
			public override string ToString ()
			{
				return "("+Item1.ToString()+","+Item2.ToString()+")";
			}
Equals()
		}

		//bad class
		public class MyID: Tuple<int, object>, ICloneable{
			public MyID(int index, object val):base(index,val){
			}
			public object Clone()
			{
				return new MyID(this.Item1, ((ICloneable)this.Item2).Clone());
			}
		}		

		Reason : serialization tends to break when used on class with many inheritance levels (reflection problems)

*/





namespace WIODatasetLanguage
{
	//Note: nunit fails in mono 30.3 (reported bug), but i do not have time now to recompile it



	class MainClass
	{

		public static void TestConcat(){
			SLogger slogger = new SLogger (logFile : "TestConcat.log");
			
			/* Load the first dataset */
			XLoudiaColorsDataset ds1 = new XLoudiaColorsDataset ();
			ds1.UpdateParameters (new XLoudiaColorsDataset.Parameters{root_path="./XloudiaColors"});
			slogger.Log ("> Built XLoudiaColorsDataset : " + ds1.Count + " elements");
			/* Load the second dataset */
			SingapourBiletsDataset ds2 = new SingapourBiletsDataset ();
			ds2.UpdateParameters (new XLoudiaColorsDataset.Parameters{root_path="./SingapourBilets"});
			Console.WriteLine ("> Built SingapourBiletsDataset : " + ds2.Count + " elements");
			/* Union them */
			ConcatDataset union = new ConcatDataset ();
			union.UpdateParameters (new ConcatDataset.Parameters{dataset1 = ds1, dataset2 = ds2});
			
			
			/*test Keys()*/
			foreach (object x in union.Keys()) { //i dont like .Keys(). it should be IDList or something like that (property)
				Console.WriteLine (x.ToString ());
			}

			NUnit.Framework.Assert.AreEqual(((
				(System.Drawing.Image)union.GetElement (new ConcatDataset.MyID(1, "daylight iPhone 4S  black background/photo 3.JPG")))
			            .Width),3264);
			
			Debug.Assert (union.Count == 73);
			Debug.Assert ((((System.Drawing.Image)
			                union.GetElement (new ConcatDataset.MyID (2,"SGD-2-b.jpg"))).Width) == 600
			              );
			
			foreach (var label in union.GetAvailableMetadata()) {
				Console.WriteLine ("Available metadata :" + label);
			}
			
			
			/* Test for more complicated scenario */
			XLoudiaColorsDataset ds3 = new XLoudiaColorsDataset ();
			ds3.UpdateParameters (new XLoudiaColorsDataset.Parameters{root_path="./XloudiaColors"});

			ConcatDataset union2 = new ConcatDataset ();
			union2.UpdateParameters (new ConcatDataset.Parameters{dataset1 = ds3, dataset2 = union});
			
			Console.WriteLine ("Union of union keys");
			foreach (object x in union2.Keys()) { //i dont like .Keys(). it should be IDList or something like that (property)
				slogger.Log (x.ToString ());
			}	

			NUnit.Framework.CollectionAssert.AreEqual(new string[]{"Void TestConcat()	(2,(2,SGD-50-b.jpg))",
				"Void TestConcat()	(2,(2,SGD-50.jpg))"}, slogger.GetLastLogs(2));

			Console.WriteLine (" ========= PASSED TEST UNION =========== ");
		}


		public static void TestLimit ()
		{
			//Tested:
			//Limit(XLoudiaColors,10))
			//Union(SingapourBilets, Limit(XLoudiaColors,10))

			SLogger slogger = new SLogger (logFile : "TestLimit.log");

			/* Load the first dataset */
			XLoudiaColorsDataset ds1 = new XLoudiaColorsDataset ();
			ds1.UpdateParameters (new XLoudiaColorsDataset.Parameters{root_path="./XloudiaColors"});
			LimitDataset ld = new LimitDataset ();
			ld.UpdateParameters (new LimitDataset.Parameters{dataset = ds1, N = 10});

			NUnit.Framework.Assert.AreEqual(ld.Count , 10);

			Console.WriteLine ("Keys are ");
			foreach (object x in ld.Keys()) { //i dont like .Keys(). it should be IDList or something like that (property)
				slogger.Log (x.ToString ());
			}

			NUnit.Framework.CollectionAssert.AreEqual(new string[]{"Void TestLimit()	daylight Galaxy s3 in Hand/20130323_054029.jpg",
				"Void TestLimit()	daylight iPhone 4S  black background/photo 1 copy.JPG",
				"Void TestLimit()	daylight iPhone 4S  black background/photo 1.JPG"}, slogger.GetLastLogs(3));


			SingapourBiletsDataset ds2 = new SingapourBiletsDataset ();
			ds2.UpdateParameters (new SingapourBiletsDataset.Parameters{root_path="./SingapourBilets"});
			ConcatDataset ud = new ConcatDataset();
			ud.UpdateParameters(new ConcatDataset.Parameters{dataset1 = ld, dataset2 = ds2});

			NUnit.Framework.Assert.AreEqual(ud.Count, 26);

			Console.WriteLine (" ========= PASSED TEST LIMIT =========== ");

		}


		public static void TestExclude ()
		{
			//Tested:
			//Exclude(XLoudiaColors, Limit(XLoudiaColors,10))

			SLogger slogger = new SLogger (logFile : "TestExclude.log");
			
			/* Load the first dataset */
			XLoudiaColorsDataset ds1 = new XLoudiaColorsDataset ();
			ds1.UpdateParameters (new XLoudiaColorsDataset.Parameters{root_path="./XloudiaColors"});
			LimitDataset ld = new LimitDataset ();
			ld.UpdateParameters (new LimitDataset.Parameters{dataset = ds1, N = 10});
			
			NUnit.Framework.Assert.AreEqual(ld.Count , 10);


			XLoudiaColorsDataset xloudia_full = new XLoudiaColorsDataset ();
			xloudia_full.UpdateParameters (new XLoudiaColorsDataset.Parameters{root_path="./XloudiaColors"});

			Console.WriteLine ("All keys are ");
			foreach (object x in xloudia_full.Keys()) { //i dont like .Keys(). it should be IDList or something like that (property)
				slogger.Log (x.ToString ());
			}
			Console.WriteLine(xloudia_full.Count);

			ExcludeDataset ex = new ExcludeDataset();
			ex.UpdateParameters(new ExcludeDataset.Parameters{dataset = xloudia_full, exdataset = ld});

			Console.WriteLine ("Keys are ");
			foreach (object x in ex.Keys()) { //i dont like .Keys(). it should be IDList or something like that (property)
				slogger.Log (x.ToString ());
			}

			NUnit.Framework.Assert.AreEqual(ex.Count, xloudia_full.Count - 10);
			NUnit.Framework.CollectionAssert.AreEqual(new string[]{"Void TestExclude()	neon iPhone 4S hand/photo 4.JPG",
				"Void TestExclude()	neon iPhone 4S hand/photo 5.JPG"}, slogger.GetLastLogs(2));


			Console.WriteLine ("Available metadata is");
			foreach(var x in ex.GetAvailableMetadata()){
				slogger.Log(x.ToString());
			}

			NUnit.Framework.CollectionAssert.AreEqual(new string[]{"Void TestExclude()	Bundle","Void TestExclude()	Label"},
			slogger.GetLastLogs(2));

			slogger.Log ("====== \t Passed *TestExclude* \t =========");
		}

		public static void TestChunk(){
			//Tested:
			//Limit(Chunk(XLoudia,4,3),2)

			SLogger slogger = new SLogger (logFile : "TestChunk.log");

			XLoudiaColorsDataset xloudia_full = new XLoudiaColorsDataset ();
			xloudia_full.UpdateParameters (new XLoudiaColorsDataset.Parameters{root_path="./XloudiaColors"});
			
			Console.WriteLine ("All keys are ");
			foreach (object x in xloudia_full.Keys()) { //i dont like .Keys(). it should be IDList or something like that (property)
				slogger.Log (x.ToString ());
			}
			Console.WriteLine(xloudia_full.Count);		


		
		
			ChunkDataset chunk = new ChunkDataset();
			chunk.UpdateParameters(new ChunkDataset.Parameters{dataset = xloudia_full, K=4, N=3}); //should return 9 10 11

			Console.WriteLine ("All keys are ");
			foreach (object x in chunk.Keys()) { //i dont like .Keys(). it should be IDList or something like that (property)
				slogger.Log (x.ToString ());
			}
			Console.WriteLine(chunk.Count);	

			NUnit.Framework.CollectionAssert.AreEqual(new string[]{"Void TestChunk()	daylight iPhone 4S  black background/photo 1.JPG",
				"Void TestChunk()	daylight iPhone 4S  black background/photo 2 copy.JPG",
				"Void TestChunk()	daylight iPhone 4S  black background/photo 2.JPG"}, slogger.GetLastLogs(3));


			LimitDataset ld = new LimitDataset();
			ld.UpdateParameters(new LimitDataset.Parameters{dataset = chunk, N=2});

			Console.WriteLine ("All keys are ");
			foreach (object x in ld.Keys()) { //i dont like .Keys(). it should be IDList or something like that (property)
				slogger.Log (x.ToString ());
			}
			Console.WriteLine(ld.Count);	

			NUnit.Framework.Assert.AreEqual(ld.Count,2);


			slogger.Log ("====== \t Passed *TestChunk* \t =========");
		}


		public static void TestFilter(){
			SLogger slogger = new SLogger (logFile : "TestFilter.log");

			/* Create test dataset */
			XLoudiaColorsDataset xloudia_full = new XLoudiaColorsDataset ();
			xloudia_full.UpdateParameters (new XLoudiaColorsDataset.Parameters{root_path="./XloudiaColors"});


			/* Filter it out */
			FilterDataset fd = new FilterDataset();


			int[] filter_indexes = {5,6,10,11,0};
			List<string> filter_ids_serialized = new List<string>();
			int counter =0 ;
			foreach(object x in xloudia_full.Keys()){
				if(filter_indexes.Contains(counter)) filter_ids_serialized.Add(JsonConvert.SerializeObject(x));
				++counter;
			}

			fd.UpdateParameters(new FilterDataset.Parameters{dataset = xloudia_full,include_list = JsonConvert.SerializeObject(filter_ids_serialized.ToArray())});

			/* Check correcctness */
		
			foreach (object x in fd.Keys()) { //i dont like .Keys(). it should be IDList or something like that (property)
				slogger.Log (x.ToString ());
			}
			NUnit.Framework.Assert.AreEqual(fd.Count,5);
			NUnit.Framework.CollectionAssert.AreEqual(new string[]{"Void TestFilter()	daylight Galaxy s3 in Hand/20130323_053939.jpg",
				"Void TestFilter()	daylight Galaxy s3 in Hand/20130323_054014.jpg",
					"Void TestFilter()	daylight Galaxy s3 in Hand/20130323_054022.jpg",
						"Void TestFilter()	daylight iPhone 4S  black background/photo 2 copy.JPG",
				"Void TestFilter()	daylight iPhone 4S  black background/photo 2.JPG"}
			, slogger.GetLastLogs(5));


			slogger.Log ("==== Passed *TestFilter*  ====");
			//TODO: test for union
		}

		public static void SerializationTest(){	
			XLoudiaColorsDataset xloudia_full = new XLoudiaColorsDataset ();
			xloudia_full.UpdateParameters (new XLoudiaColorsDataset.Parameters{root_path="./XloudiaColors"});
			
			XLoudiaColorsDataset xloudia_full2 = new XLoudiaColorsDataset ();
			xloudia_full2.UpdateParameters (new XLoudiaColorsDataset.Parameters{root_path="./XloudiaColors"});
			
			ConcatDataset union = new ConcatDataset();
			union.UpdateParameters(new ConcatDataset.Parameters{dataset1 = xloudia_full, dataset2 = xloudia_full2});
			
			foreach(object id in union.Keys()){
				Console.WriteLine(JsonConvert.SerializeObject(id));
				Console.WriteLine(id.GetType().Name);
				Console.WriteLine (JsonConvert.DeserializeObject(JsonConvert.SerializeObject(id),typeof(ConcatDataset.MyID)));
				Console.WriteLine(id.ToString());
			}	
		}


		public static void TestShuffle(){

			SLogger slogger = new SLogger (logFile : "TestShuffle.log");
			/* Create test dataset */
			XLoudiaColorsDataset xloudia_full = new XLoudiaColorsDataset ();
			xloudia_full.UpdateParameters (new XLoudiaColorsDataset.Parameters{root_path="./XloudiaColors"});
			
			ShuffleDataset shuffle = new ShuffleDataset();
			shuffle.UpdateParameters(new ShuffleDataset.Parameters{dataset = xloudia_full, seed = 1});

			foreach (object x in shuffle.Keys()) { //i dont like .Keys(). it should be IDList or something like that (property)
				slogger.Log (x.ToString ());
			}

			NUnit.Framework.Assert.AreEqual(shuffle.Count, xloudia_full.Count);
			NUnit.Framework.Assert.AreEqual(slogger.GetLastLogs(1)[0], "Void TestShuffle()	daylight iPhone 4S  black background/photo 5.JPG");

			slogger.Log ("==== Passed *TestShuffle*  ====");
		}

		public static void TestZip(){
			//Test: zip(xloudia, shuffle(sourceCodes))

			SLogger slogger = new SLogger (logFile : "TestZip.log");
			
			/* Create test dataset - xloudia dataset */
			XLoudiaColorsDataset xloudia_full = new XLoudiaColorsDataset ();
			xloudia_full.UpdateParameters (new XLoudiaColorsDataset.Parameters{root_path="./XloudiaColors"});

			/* Create second test dataset - some source codes */
			FileDataset fd = new FileDataset();
			fd.UpdateParameters(new FileDataset.Parameters{root_path = "./SourceCodes"});

			slogger.Log (fd.Count);

			ShuffleDataset sh = new ShuffleDataset();
			sh.UpdateParameters(new ShuffleDataset.Parameters{dataset = fd, seed = 1});

			slogger.Log(sh.Count);


			fd.QueryGetElementType();



			NUnit.Framework.Assert.AreEqual("String",fd.QueryGetElementType().Name );
			NUnit.Framework.Assert.AreEqual("String",sh.QueryGetElementType().Name );
			NUnit.Framework.Assert.AreEqual(sh.Count, fd.Count);

			ZipDataset zip = new ZipDataset();
			zip.UpdateParameters(new ZipDataset.Parameters{dataset1=xloudia_full, dataset2 = sh});
			slogger.Log ("Zip keys are");
			foreach (object x in zip.Keys()) { //i dont like .Keys(). it should be IDList or something like that (property)
				slogger.Log (x.ToString ());
				object y = zip.GetElement(x);
				slogger.Log (((Tuple<System.Drawing.Image, string>)y).Item1.ToString());
				slogger.Log (y.GetType().Name);
				slogger.Log (zip.QueryGetElementType().Name);
				slogger.Log (zip.QueryGetElementType().InvokeMember(null, System.Reflection.BindingFlags.CreateInstance,null,null,new object[]{null,null}).GetType().Name); 
			}
			NUnit.Framework.Assert.AreEqual(1, zip.Count);
			NUnit.Framework.CollectionAssert.AreEqual(new string[]{
				"Void TestZip()	daylight Galaxy s3 in Hand/20130323_054002.jpg",
					"Void TestZip()	System.Drawing.Bitmap",
						"Void TestZip()	Tuple`2",
						"Void TestZip()	Tuple`2",
				"Void TestZip()	Tuple`2"}, slogger.GetLastLogs(5));



			slogger.Log ("==== Passed *TestZip*  ====");
		}

		public static void TestFileDataset(){
			SLogger slogger = new SLogger (logFile : "TestFileDataset.log");
			FileDataset fd = new FileDataset();
			fd.UpdateParameters(new FileDataset.Parameters{root_path = "./SourceCodes"});



			foreach (object x in fd.Keys()) { //i dont like .Keys(). it should be IDList or something like that (property)
				slogger.Log (x.ToString ());
			}


			NUnit.Framework.Assert.AreEqual(fd.GetElement ("include/DebugCollector.h")
				,"LyogCiAqIEZpbGU6ICAgRGVidWdDb2xsZWN0b3IuaAogKiBBdXRob3I6IHN0Y" +
					"XN6ZWsKICoKICogQ3JlYXRlZCBvbiAxMCBzaWVy" +
					"cGllxYQgMjAxMiwgMTg6MjQKICovCgojaWZuZGVmIERFQlV" +
					"HQ09MTEVDVE9SX0gKI2RlZmluZSBERUJVR0NPTExFQ1RPUl9ICgoK" +
					"I2luY2x1ZGUgPGFsZ29yaXRobT4KI2luY2x1ZGUgPGlvc3RyZWFtPgojaW5jbH" +
					"VkZSA8c3N0cmVhbT4KI2luY2x1ZGUgPHN0cmluZz4KI2luY2x1ZGUgPHZlY3Rvcj4KC" +
					"iNkZWZpbmUgREVCVUdfQ09MTEVDVE9SX0RFQlVHCgpjbGFzcyBEZWJ1Z0NvbGxlY3Rv" +
					"ciB7CiAgICAKICAgIHN0cnVjdCBSZWNvcmR7CiAgICAgICAgc3RkOjpzdHJpbmcgbGluZ" +
					"TsKICAgICAgICBpbnQgbGV2ZWw7CiAgICAgICAgUmVjb3JkKGludCBsZXZlbCwgc3RkOjpzd" +
					"HJpbmcgbGluZSk6bGluZShsaW5lKSxsZXZlbChsZXZlbCkKICAgICAgICB7fQogICAgICAgIGZy" +
					"aWVuZCBzdGQ6Om9zdHJlYW0gJiBvcGVyYXRvcjw8KHN0ZDo6b3N0cmVhbSAmIG91dCwgY29uc3QgU" +
					"mVjb3JkICYgcil7IAogICAgICAgICAgICBzdGQ6OnN0cmluZyBhID0gci5saW5lOwogICAgICAgICAg" +
					"ICBvdXQ8PGE7CiAgICAgICAgICAgIHJldHVybiBvdXQ7CiAgICAgICAgfQogICAgfTsKICAgIHN0Z" +
					"Do6dmVjdG9yPFJlY29yZD4gcmVjb3JkczsgCiAgICBpbnQgbV9kZWJ1Z19sZXZlbDsKcHVibGlj" +
					"OgogICAgRGVidWdDb2xsZWN0b3IoaW50IGRlYnVnX2xldmVsPTApOiBtX2RlYnVnX2xldmVsKGRlYnV" +
					"nX2xldmVsKXt9CiAgICBzdGQ6OnN0cmluZyByZXBvcnQoaW50IGN1dHRpbmdfbGV2ZWw9MCl7CiAgI" +
					"CAgICAgdXNpbmcgbmFtZXNwYWNlIHN0ZDsKICAgICAgICBzdHJpbmdzdHJlYW0gc3M7CiAgICAgI" +
					"CAgCiAgICAgICAgc3M8PCJERUJVRyBSRVBPUlRcbiAtLS0tLS0tLS0gXG4iOwogICAgICAgIAogI" +
					"CAgICAgIGZvcihpbnQgaT0wO2k8KGludClyZWNvcmRzLnNpemUoKTsrK2kpewogICAgICAgICAgIC" +
					"BpZihyZWNvcmRzW2ldLmxldmVsPj1jdXR0aW5nX2xldmVsKSBzczw8cmVjb3Jkc1tpXS5saW5lPDx" +
					"zdGQ6OmVuZGw7CiAgICAgICAgfQoKICAgICAgICByZXR1cm4gc3Muc3RyKCk7CiAgICB9CgogICAg" +
					"dm9pZCBzZXRfZGVidWdfbGV2ZWwoaW50IGRlYnVnX2xldmVsKXsgbV9kZWJ1Z19sZXZlbCA9IGRlY" +
					"nVnX2xldmVsOyB9CiAgICB2b2lkIHB1c2hfYmFjayhpbnQgbGV2ZWwsIHN0ZDo6c3RyaW5nIGxpbmU" +
					"pewogICAgICAgICAgICAgICAgaWYobGV2ZWw+PSBtX2RlYnVnX2xldmVsKSBzdGQ6OmNvdXQ8PGxpb" +
					"mU8PHN0ZDo6ZW5kbDw8c3RkOjpmbHVzaDsKICAgIH0KcHJpdmF0ZToKCn07CgojZW5kaWYJLyogRE" +
					"VCVUdDT0xMRUNUT1JfSCAqLwoK");

			Console.WriteLine("=== Passed *TestFileDataset* ===");
		}


		public static void TestProduct(){
			SLogger slogger = new SLogger (logFile : "TestProduct.log");


			/* Load the first dataset */
			XLoudiaColorsDataset ds1 = new XLoudiaColorsDataset ();
			ds1.UpdateParameters (new XLoudiaColorsDataset.Parameters{root_path="./XloudiaColors"});
			slogger.Log ("> Built XLoudiaColorsDataset : " + ds1.Count + " elements");
			/* Load the second dataset */
			SingapourBiletsDataset ds2 = new SingapourBiletsDataset ();
			ds2.UpdateParameters (new XLoudiaColorsDataset.Parameters{root_path="./SingapourBilets"});
			Console.WriteLine ("> Built SingapourBiletsDataset : " + ds2.Count + " elements");
			/* Product them */
			ProductDataset pd = new ProductDataset();
			pd.UpdateParameters(new ProductDataset.Parameters{dataset1 = ds1, dataset2 = ds2});

			NUnit.Framework.Assert.AreEqual(pd.Count, ds2.Count*ds1.Count);

			slogger.LogArray(pd.GetAvailableMetadata());

			NUnit.Framework.Assert.AreEqual("Void TestProduct()	(Bundle,Label,Bundle,)\n",slogger.GetLastLogs(1)[0]);


			//test product with an empty dataset
			XLoudiaColorsDataset ds3 = new XLoudiaColorsDataset();
			pd.UpdateParameters(new ProductDataset.Parameters{dataset1 = ds1, dataset2 = ds3});

			NUnit.Framework.Assert.AreEqual(pd.Count,0);


			Console.WriteLine("=== Passed *TestProduct* ===");
		}
		public static void TestHierarchical(){
			SLogger slogger = new SLogger(logFile:"TestHierarchical.log");
			
			XLoudiaColorsDataset ds1 = new XLoudiaColorsDataset ();
			ds1.UpdateParameters (new XLoudiaColorsDataset.Parameters{root_path="./XloudiaColors"});
			
			
			foreach(object id in ds1.Keys ()){
				slogger.Log (((Dictionary<string,object>)ds1.GetMetadataBundle(id))["color"]);
			}
			
			NUnit.Framework.Assert.AreEqual(slogger.GetLastLogs(1)[0],"Void TestHierarchical()	Finezzo");
			
			slogger.Log("====== *Passed TestHierarchical* ======");
		}


		public static void TestMongo(){
			SLogger slogger = new SLogger("TestMongo.log");

			MongoDataset mongo = new MongoDataset();
			mongo.UpdateParameters(new MongoDataset.Parameters{db = "aiosciweb1", collection = "country"});

			foreach (object x in mongo.Keys()) { //i dont like .Keys(). it should be IDList or something like that (property)
				slogger.Log (x.ToString ());
			}

			slogger.Log (mongo.GetElement(new BsonString("VN")));

			NUnit.Framework.Assert.AreEqual(slogger.GetLastLogs(1)[0],"Void TestMongo()\t{ \"_id\" : \"VN\", \"name\" : \"VIET NAM\", \"printable_name\" : \"Viet Nam\", \"numcode\" : 704, \"iso3\" : \"VNM\" }");

			slogger.Log ("===== Passed *TestMongo* ======");

		}


		public static void TestTrainingTestingDataset(){
		
			SLogger slogger = new SLogger (logFile : "TestTrainingTestingDataset.log");
			
			/* Create test dataset - xloudia dataset */
			XLoudiaColorsDataset xloudia_full = new XLoudiaColorsDataset ();
			xloudia_full.UpdateParameters (new XLoudiaColorsDataset.Parameters{root_path="./XloudiaColors"});

			int lim = 10;
			int cnt = 0;

			foreach(var x in xloudia_full.Keys()) {
				if(cnt++ > lim) break;
				Console.WriteLine(x.ToString());
			}


			TrainingDataset td = new TrainingDataset();
			td.UpdateParameters(new TrainingDataset.Parameters{source_dataset = xloudia_full, seed = 2, training_percentage = 0.6});

			lim = 100;
			cnt = 0;
			foreach(var x in td.Keys()) {
				if(cnt++ > lim) break;
				Console.WriteLine(x.ToString());
			}

			NUnit.Framework.Assert.AreEqual(34, td.Count);

			slogger.Log("======== Passed *TestTrainingTestingDataset* ==========");

		}


		public static void TestWhole(){
		
			SLogger slogger = new SLogger (logFile : "TestWhole.log");
			FileDataset fd = new FileDataset();
			fd.UpdateParameters(new FileDataset.Parameters{root_path = "./SourceCodes"});


			WholeDataset whole = new WholeDataset();
			whole.UpdateParameters(new WholeDataset.Parameters{dataset = fd});

			foreach(var id in whole.Keys()){
				slogger.Log (id.ToString());
			}
			slogger.LogArray(whole.GetAvailableMetadata());
			List<object> x = (List<object>)whole.GetMetaByLabel("whole", "Path");
			slogger.LogArray(x.ToArray());

			NUnit.Framework.Assert.AreEqual("Void TestWhole()	(./SourceCodes/Makefile,./Sou" +
				"rceCodes/README.md,./SourceCodes/TODO.txt,./SourceCodes/main,./SourceCodes/b" +
				"uild/performance/GNGAlgorithm.o,./SourceCodes/build/performance/SHGraphDefs." +
				"o,./SourceCodes/build/performance/SHMemoryManager.o,./SourceCodes/build/perfor" +
				"mance/Utils.o,./SourceCodes/daylight Galaxy s3 in Hand/20130323_054002.jpg,./S" +
				"ourceCodes/doc/notes,./SourceCodes/doc/todonow,./SourceCodes/include/DebugColl" +
				"ector.h,./SourceCodes/include/ExtGraphNodeManager.h,./SourceCodes/include/Ext" +
				"MemoryManager.h,./SourceCodes/include/GNG.h,./SourceCodes/include/GNGAlgorith" +
				"m.h,./SourceCodes/include/GNGAlgorithmControl.h,./SourceCodes/include/GNGData" +
				"base.h,./SourceCodes/include/GNGDatabase.h~,./SourceCodes/include/GNGDefines" +
				".h,./SourceCodes/include/GNGGlobals.h,./SourceCodes/include/GNGGraph.h,./So" +
				"urceCodes/include/GNGLazyErrorHeap.h,./SourceCodes/include/Heap.h,./Source" +
				"Codes/include/RcppInterface.h,./SourceCodes/include/SHGraphDefs.h,./SourceC" +
				"odes/include/SHMemoryManager.h,./SourceCodes/include/UniformGrid.h,./Source" +
				"Codes/include/Utils.h,./SourceCodes/screens/006.png,./SourceCodes/screens/0" +
				"06b.png,./SourceCodes/screens/1.png,./SourceCodes/screens/4.png,./SourceCod" +
				"es/screens/fig10.png,./SourceCodes/screens/fig11.png,./SourceCodes/screens/fi" +
				"g9_both.png,./SourceCodes/screens/kowalska_miaraBF.png,./SourceCodes/screens/pl" +
				"ot.png,./SourceCodes/screens/plot_speed.png,./SourceCodes/screens/porownani" +
				"e.png,./SourceCodes/screens/v002,./SourceCodes/screens/v004.png,./SourceCode" +
				"s/screens/v005,./SourceCodes/screens/v005.png,./SourceCodes/screens/v005b,./S" +
				"ourceCodes/screens/v007.png,./SourceCodes/screens/v007b.png,./SourceCodes/scr" +
				"eens/v008,./SourceCodes/screens/v009.png,./SourceCodes/screens/v009a.png,./So" +
				"urceCodes/screens/v012.png,./SourceCodes/screens/v013.png,./SourceCodes/scree" +
				"ns/v13b.png,./SourceCodes/scripts/GNGConvertToIGraph.r,./SourceCodes/scripts/" +
				"GNGVisualisePoints.r,./SourceCodes/scripts/GNGVisualisePoints.r~,./SourceCod" +
				"es/scripts/GNGWriteToObj.r,./SourceCodes/scripts/RcppInterface.o,./SourceCod" +
				"es/scripts/RcppInterface.r,./SourceCodes/scripts/RcppInterface.r~,./SourceC" +
				"odes/scripts/RcppInterface.so,./SourceCodes/scripts/VisualisePoints.r~,./So" +
				"urceCodes/scripts/compile.r~,./SourceCodes/scripts/generateflags.r,./SourceC" +
				"odes/scripts/generateflags.r~,./SourceCodes/scripts/gng.r,./SourceCodes/scri" +
				"pts/gng.r~,./SourceCodes/scripts/slowcheckperformance.r~,./SourceCodes/scrip" +
				"ts/sphere.r~,./SourceCodes/scripts/writetoobj_lines.r~,./SourceCodes/script" +
				"s/Archiwum/1.r,./SourceCodes/scripts/Archiwum/LoadPotential.r,./SourceCodes" +
				"/scripts/Archiwum/RcppInterface_Visualise.r,./SourceCodes/scripts/Archiwum/" +
				"RcppInterface_Visualise_Animation.r,./SourceCodes/scripts/Archiwum/RcppIn" +
				"terface_Visualise_Points.r,./SourceCodes/scripts/Archiwum/RcppInterface_l" +
				"earning_curve.r,./SourceCodes/scripts/Archiwum/RcppInterface_load_potenti" +
				"al.r,./SourceCodes/scripts/Archiwum/VisualisePoints.r,./SourceCodes/script" +
				"s/Archiwum/VisualisePointsSingle.r,./SourceCodes/scripts/Archiwum/Visuali" +
				"sePointsSingleTraditional.r,./SourceCodes/scripts/Archiwum/boxdemo.r,./Sour" +
				"ceCodes/scripts/Archiwum/boxdemo.r~,./SourceCodes/scripts/Archiwum/checkpe" +
				"rformance.r,./SourceCodes/scripts/Archiwum/compile.r,./SourceCodes/scripts" +
				"/Archiwum/data,./SourceCodes/scripts/Archiwum/die.r,./SourceCodes/scripts/Archiwum/gng.r,./So" +
				"urceCodes/scripts/Archiwum/mat,./SourceCodes/scripts/Archiwum/plottest.r,./SourceCodes/scripts" +
				"/Archiwum/setserver.r,./SourceCodes/scripts/Archiwum/slowcheckperformance.r,./SourceCodes/" +
				"scripts/Archiwum/slowcheckperformance.r~,./SourceCodes/scripts/Archiwum/sphere.r,./SourceC" +
				"odes/scripts/Archiwum/sphereFunction.r,./SourceCodes/scripts/Archiwum/test.r,./SourceCod" +
				"es/scripts/Archiwum/tmp,./SourceCodes/scripts/Archiwum/writetoobj.r,./SourceCodes/scrip" +
				"ts/Archiwum/writetoobj_lines.r,./SourceCodes/scripts/Archiwum/writetoobj_setserver.r,." +
				"/SourceCodes/src/ExtGraphNodeManager.hpp,./SourceCodes/src/ExtGraphNodeManager.hpp~,." +
				"/SourceCodes/src/GNGAlgorithm.cpp,./SourceCodes/src/RcppInterface.cpp,./SourceCodes" +
				"/src/RcppInterface.cpp~,./SourceCodes/src/SHGraphDefs.cpp,./SourceCodes/src/SHMem" +
				"oryManager.cpp,./SourceCodes/src/UniformGrid.hpp,./SourceCodes/src/Utils.cpp,./S" +
			                                "ourceCodes/src/main.cpp,./SourceCodes/src/main.cpp~,)\n", slogger.GetLastLogs(1)[0]);


		
			List<string> whole_element = (List<string>)whole.GetElement("whole");

			slogger.Log(whole_element.Count);

			NUnit.Framework.Assert.AreEqual(110, whole_element.Count);

			slogger.Log("========= Passed *TestWhole* ==========");

		}

//		public static ShowImageDialog shd;
//		public static void gui_do(){
//			Application.Init();
//		
//
//			shd = new ShowImageDialog(img:Utils.ImageToPixbuf(
//				System.Drawing.Bitmap.FromFile("./img.jpg")));	
//			shd.ShowAll();
//			Application.Run ();
//		}
		
		
		public static void TestImageFilter (bool display_keys = false)
		{
			SLogger slogger = new SLogger (logFile : "TestImageFilter.log");
			/* Create test dataset */
			XLoudiaColorsDataset xloudia_full = new XLoudiaColorsDataset ();
			xloudia_full.UpdateParameters (new XLoudiaColorsDataset.Parameters{root_path="./XloudiaColors"});	
		
			
			ImageFilterDataset img_filter_scale = new ImageFilterDataset();
			img_filter_scale.UpdateParameters(new ImageFilterDataset.Parameters{
				dataset = xloudia_full, 
				filter =ImageFilter.FILTER_RESIZE, 
				filter_params = new object[]{0.1}});
			
			
			ImageFilterDataset img_filter = new ImageFilterDataset();
			img_filter.UpdateParameters(new ImageFilterDataset.Parameters{
				dataset = img_filter_scale, 
				filter = ImageFilter.FILTER_BRIGHTNESS, 
				filter_params = new object[]{2.0}});

			
			ImageFilterDataset img_filter_crop = new ImageFilterDataset();
			img_filter_crop.UpdateParameters(new ImageFilterDataset.Parameters{
				dataset = img_filter, 
				filter = ImageFilter.FILTER_CROP, 
				filter_params = new object[]{100,100,50,0}});
			
			ImageFilterDataset img_filter_flip = new ImageFilterDataset();
			img_filter_flip.UpdateParameters(new ImageFilterDataset.Parameters{
				dataset = img_filter, 
				filter = ImageFilter.FILTER_VFLIP, 
				filter_params = null});	
			
			if(display_keys){
				//The name of the window
				String win1 = "Test Window";
	
				//Create the window using the specific name
				CvInvoke.cvNamedWindow (win1);			
				foreach (object x in img_filter_flip.Keys()) {
				
	
	 
					//Create an image of 400x200 of Blue color
	
					///Image<Bgr, Byte> img = new Image<Bgr, Byte> (((System.Drawing.Image)xloudia_full.GetElement (x)));
					System.Drawing.Image imgcsharp = ((System.Drawing.Image)img_filter_flip.GetElement (x));
					System.Drawing.Bitmap bmpImage = new System.Drawing.Bitmap(imgcsharp);
		
					Image<Bgr, Byte> img = new Image<Bgr,Byte>(bmpImage);
	
					CvInvoke.cvShowImage (win1, img.Ptr);
					//Wait for the key pressing event
					//Thread.Sleep (1000);
					//Destory the window
					
					CvInvoke.cvWaitKey(10);
				}
				CvInvoke.cvDestroyWindow (win1); 
			}
				
			slogger.Log("========= Passed *TestImageFilter* ==========");

		}
	
		public static void test(double x){
		
		}
		public static void TestImageFilterStar (bool display_keys1 = false, bool display_keys2 = false)
		{
			SLogger slogger = new SLogger (logFile : "TestImageFilterStar.log");
			
			
			/* Create test dataset */
			ImageDataset singapour_bilets = new ImageDataset ();
			singapour_bilets.UpdateParameters (new ImageDataset.Parameters{root_path="./SingapourBilets"});	
			
			if(display_keys2){
				String win1 = "Test Window";
				CvInvoke.cvNamedWindow (win1);			
				foreach (object x in singapour_bilets.Keys()) {
					System.Drawing.Image imgcsharp = ((System.Drawing.Image)singapour_bilets.GetElement (x));
					System.Drawing.Bitmap bmpImage = new System.Drawing.Bitmap(imgcsharp);
		
					Image<Bgr, Byte> img = new Image<Bgr,Byte>(bmpImage);
	
					CvInvoke.cvShowImage (win1, img.Ptr);

					CvInvoke.cvWaitKey(200);
				}
				CvInvoke.cvDestroyWindow (win1); 
			}	
			
			/* Resize images dataset */
			ImageFilterDataset img_filter_scale = new ImageFilterDataset();
			img_filter_scale.UpdateParameters(new ImageFilterDataset.Parameters{
				dataset = singapour_bilets, 
				filter =ImageFilter.FILTER_RESIZE, 
				filter_params = new object[]{0.7}});
			
			
			/* Crop images dataset */
			ImageFilterDataset img_filter_crop = new ImageFilterDataset();
			img_filter_crop.UpdateParameters(new ImageFilterDataset.Parameters{
				dataset = img_filter_scale, 
				filter = ImageFilter.FILTER_CROP, 
				filter_params = new object[]{100,100,50,0}});
			

			/* Create variations of images dataset */
			ImageFilterStarDataset img_filter_brightness_explode = new ImageFilterStarDataset();
			img_filter_brightness_explode.UpdateParameters(new ImageFilterStarDataset.Parameters{
				dataset = img_filter_scale, 
				filter = ImageFilter.FILTER_BRIGHTNESS, 
				filter_params = new object[]{0.1,3.0},
				count = 10,
				seed = 2
			});			

			
			ImageFilterStarDataset img_filter_brightness_explode2 = new ImageFilterStarDataset();
			img_filter_brightness_explode2.UpdateParameters(new ImageFilterStarDataset.Parameters{
				dataset = img_filter_brightness_explode, 
				filter = ImageFilter.FILTER_ROTATION, 
				filter_params = new object[]{-90.0,90.0,255.0,255.0,255.0,255.0,255.0,255.0},
				count = 10,
				seed = 2
			});			
			
			test (5);
		
			
			/* Shuffle it */
			ShuffleDataset sh = new ShuffleDataset();
			sh.UpdateParameters(new ShuffleDataset.Parameters{ dataset = img_filter_brightness_explode2, seed = 3});
			
			
			
			if(display_keys1){
				String win1 = "Test Window";
				CvInvoke.cvNamedWindow (win1);			
				foreach (object x in sh.Keys()) {
					System.Drawing.Image imgcsharp = ((System.Drawing.Image)sh.GetElement (x));
					System.Drawing.Bitmap bmpImage = new System.Drawing.Bitmap(imgcsharp);
		
					Image<Bgr, Byte> img = new Image<Bgr,Byte>(bmpImage);
	
					CvInvoke.cvShowImage (win1, img.Ptr);

					CvInvoke.cvWaitKey(200);
				}
				CvInvoke.cvDestroyWindow (win1); 
			}
				
			slogger.Log("========= Passed *TestImageFilterStar* ==========");

			
			//all this manipulation could have been a single command in json
		}	
		
		
		public static void Main (string[] args)
		{

//			Thread gui_thread = new Thread(new ThreadStart(gui_do));
//			gui_thread.Start();
//
//			Thread.Sleep(1000);
//			shd.img = Utils.ImageToPixbuf(
//			System.Drawing.Bitmap.FromFile("./img2.jpg"));
//	
//			GLib.Idle.Add(new GLib.IdleHandler(shd.ChangeImage));

			
		//	CvInvoke.cvNamedWindow("TestWindow");
			
			try{
				TestHierarchical();
				TestConcat();
				TestLimit();
				TestExclude();
				TestChunk();
				TestFilter();
				TestShuffle();
				TestFileDataset(); 
				TestZip();
				TestProduct();
				//TestMongo (); //Requires MongoDB running
				//TODO: test multilevel utility dataset and GetMetadata query (best on some tuple like, for instance zip?)
				TestTrainingTestingDataset();
				TestWhole();
				
				
				TestImageFilter(false);
				TestImageFilterStar(true, true);
			}
			catch(Exception e){
				Console.WriteLine("=== Test Fail ===");
				Console.WriteLine(e.ToString());
			}



	
			//SerializationTest();

		}
	}
}
