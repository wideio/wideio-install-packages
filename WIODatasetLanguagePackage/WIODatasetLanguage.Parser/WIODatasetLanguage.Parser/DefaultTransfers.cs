using System;
using System.Text.RegularExpressions;
using System;
using System.IO;
using System.Collections.Generic;
using System.Reflection;
//myregex([\s,])([A-Za-z_][A-Z0-9a-z_])*)(\()

using WIODataLanguage;

namespace WIODatasetLanguage
{


	/// <summary>
	/// <summary>
	/// Empty transfer just for documentation purposes
	/// </summary>
	/// </summary>
	[WIOCallnameAnnotation("EmptyTransfer")]
	public class EmptyTransfer: ITransfer{
		public override string[] Transfer (string[] input)
		{
			return input;		
		}	
	}
}

