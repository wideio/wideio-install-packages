using System;
using System.Diagnostics;
using System.Collections;
using System.IO;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
//using newtonsof

namespace WIODatasetLanguage
{
    class Utils
	{
		public static SLogger MainLogger = new SLogger("log");	
		
		public static T Cast<T>(object o)
        {
            return (T)o;
        }
		
		public static void SerializeCollection<T>( IEnumerable<T> collection){
			string output = "";
			foreach(var x in collection) output+=x.ToString()+",";
			output += "\n";
		}		
		public static void WriteCollection<T>( IEnumerable<T> collection){
			foreach(var x in collection) Console.Write(x.ToString()+",");
			Console.Write("\n");
		}
		/// <summary>
		/// Clone the specified collection , WARNING : can be possibly a shallow copy!!
		/// </summary>
		public static IEnumerable<T> Clone<T>( IEnumerable<T> collection) where T : ICloneable
		{
			return collection.Select (item => (T)item.Clone ());
		}
		/// <summary>
		/// Serialize_object util function
		/// </summary>
		/// <param name='obj'>
		/// Object to be serialized
		/// </param>
		public static string serialize_object (Object obj)
		{
			return JsonConvert.SerializeObject (obj);
		}
		
		/// <summary>
		/// Deserialize_object util function
		/// </summary>
		/// <param name='obj'>
		/// String of deserialized object
		/// </param>
		public static T deserialize_object<T> (string serialization) where T: class
		{
			return JsonConvert.DeserializeObject<T>(serialization);
		}
	}
}
