﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.Diagnostics;
using System.IO;

namespace WIODatasetLanguage
{
    /// <summary>
    /// Simple class for logging (I need it for unit tests)
	/// TODO: Add decorators
    /// </summary>
    class SLogger
    {

		List<string> logs = new List<string>();
		string formatScheme, logFile;
		bool displayLogs;
		
		
		public SLogger (string logFile = "", string formatScheme = "FormatMessageDefaultNoTime", bool displayLogs = true, bool flushLogFile = true)
		{
			this.formatScheme = formatScheme;
			this.logFile = logFile;
			
			
			
			this.displayLogs = displayLogs;
		}
		public void Log (object message, bool new_line = true, StackFrame callstack = null)
		{
			if (callstack == null)
				callstack = new StackFrame (1, true);
			
		
			string formatted_message = (string)this.GetType ().GetMethod (this.formatScheme).Invoke (this, new object[]{callstack, message.ToString()});
			
			if (this.displayLogs){
				if(new_line)
					Console.WriteLine (formatted_message);
				else
					Console.Write(formatted_message);
			}
			
			this.LogToList (formatted_message, new_line);
			
			if (this.logFile != "")
				this.LogToFile (formatted_message, new_line);
		}
	    public void LogArray (object[] array)
		{
			string message = "(";
			foreach (var t in array)
				message+=(t.ToString () + ",");
            message+=")\n";
			this.Log (message, true, new StackFrame (1, true));
        }
			
		/// <summary>
		/// Gets the last logs
		/// </summary>
		public string[] GetLastLogs (int N)
		{
			return this.logs.GetRange (logs.Count - N , N).ToArray();
		}
			
		public string FormatMessageDefault (StackFrame callstack, string message)
		{
			return string.Format (
				"{0} {1}:{2}\t{3}",
				DateTime.Now.ToString (),
				callstack.GetFileLineNumber (),
				callstack.GetMethod (),
				message
			);		
		}	
		public string FormatMessageDefaultNoTime (StackFrame callstack, string message)
		{
			return string.Format (
				"{0}\t{1}",
				callstack.GetMethod().Name,
				message
			);		
		}		
		
		private void LogToList (string message, Boolean newLine = true)
		{
			if (!newLine) {
				if (logs.Count == 0)
					logs.Add (message);
				else 
					logs[logs.Count-1] += message;
			} else {
				logs.Add (message);
			}
		}
		
        private void LogToFile (string message, Boolean newLine = true)
		{
			if (!File.Exists (this.logFile)) {
				FileStream tmp1 = File.Create (this.logFile);
				tmp1.Close ();
			}

			FileStream fs = new FileStream (this.logFile, FileMode.Open, FileAccess.Write, FileShare.ReadWrite);
            StreamWriter sw = new StreamWriter(fs);
            
            fs.Seek(0, SeekOrigin.End);
            
            if (newLine)
                sw.WriteLine(message);
            else
                sw.Write(message);
            
            sw.Close();
            fs.Close();
        }

    }
}