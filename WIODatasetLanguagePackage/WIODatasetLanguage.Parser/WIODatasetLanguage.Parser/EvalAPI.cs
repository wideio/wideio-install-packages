using System;
namespace WIODatasetLanguage
{
	/// <summary>
	/// Functions called by code modified by transfers to circumvent limited Eval capibilities
	/// Note : cannot be static because C#Eval doesn't accept static objects
	/// </summary>
	public class EvalAPIWrapper
	{
		WIODatasetLanguageEnv env; //for calling
		
		public EvalAPIWrapper (WIODatasetLanguageEnv env)
		{
			this.env = env;
		}
		

	}
}

