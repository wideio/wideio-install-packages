using System;
using System;
using System.IO;
using System.Collections.Generic;
using System.Reflection;
using WIODataLanguage;

namespace WIODatasetLanguage
{
	/// <summary>
	/// Command to load database, returns database object
	/// </summary>
	[WIOCallnameAnnotation("load")]
	public class LoadCommand : ICommand{
		public override object Execute (params object[] parameters)
		{
			return "Hello";
		}
	}
	
	[WIOCallnameAnnotation("hello")]
	public class HelloCommand : ICommand{
		public override object Execute (params object[] parameters)
		{
			return "Hello world 2";
		}
		public object Execute2 (object[] parameters)
		{
			return "Hello world";
		}
	}	
}

