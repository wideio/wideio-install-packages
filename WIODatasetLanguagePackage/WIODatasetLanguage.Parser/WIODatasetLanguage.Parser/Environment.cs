using System;
using System.IO;
using System.Collections.Generic;
using System.Reflection;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

using WIODataLanguage; //i will refactor it i promise..

//Note : we are heavily dependent on Newtonsoft.JSON libraries capabilities (recursive conversion),
//but it is a mature library so I guess it is ok

//TODO: add config file to specify on execution load directories and order of transfer functions
//TODO: to make it work everything has to be public, how to do it without C++ friend mechanism? method for getting methods using reflection? 
//TODO: create IWIODatasetLanguageEnv, and move it to the global dll, along with generic hierarchical dataset and being created union dataset

namespace WIODatasetLanguage
{
        // ./datasets : datasets to be loaded automatically
        // ./commands : commands dll to be loaded automatically - not implemented yet
        
        
        public abstract class IWIODatasetLanguageEnv{
                public abstract object Evaluate(string command);
                
        }
        
        /// <summary>
        /// WIO data language environment - load assemblies, keep track of global variables etc.
        /// </summary>
        public class WIODatasetLanguageEnv
        {
                EvalAPIWrapper API;


                private List<Assembly> assemblies = new List<Assembly>();
                
                //for indexing purposes
                //commands not used at the moment
                public Dictionary<string, ICommand> callbackCommands = new Dictionary<string,ICommand>();
                public Dictionary<string, ITransfer> callbackTransfers = new Dictionary<string,ITransfer>();
                public Dictionary<string, Type> callbackDataset = new Dictionary<string,Type>();
                public Dictionary<string, IConstraint> callbackConstraint = new Dictionary<string, IConstraint>();

                
                
                
                public Dictionary<string, bool> nameRegistrationDict = new Dictionary<string, bool>();


                public List<string> defaultDirectories = new List<string>();

                
                public class WIODatasetLanguageException : System.Exception{
                        public string exception;
                        public WIODatasetLanguageException(string exception = "Default exception"){
                                this.exception = exception;
                                
                        }
                        public override string ToString ()
                        {
                                return this.exception;
                        }
                }
                

                
                WIODatasetLanguageEnv (string home_path = ".")
                {
                        this.API = new EvalAPIWrapper (this); //IT should be static, but C#Eval doesn't accept static classes
                        this.LoadAssembly (Assembly.GetExecutingAssembly ());
//                        CsEval.EvalEnvironment = this; //TODO: Make special environment as here we are using dangerous using imports
                        
						if(home_path.EndsWith("/")){
							defaultDirectories.Add (home_path+"datasets");
	                        defaultDirectories.Add (home_path+"extensions");								
						}else{
							defaultDirectories.Add (home_path+"/datasets");
	                        defaultDirectories.Add (home_path+"/extensions");					
						}	
			
						foreach (var d in this.defaultDirectories)
                                this.RecursivelyLoadAssemblies (d);
                }
		
			
		
		
                private static WIODatasetLanguageEnv instance = null;
                public void Evaluate (string[] commands)
                {
                        foreach (var command in commands)
                                this.Evaluate (command);
                }
                public object Evaluate (string command)
                {
                        
						
			
			
						string[] commands_partitioned = command.Split('\n');
						//it is a prototype of multiline command with new lines, it might be a bad idea sometimes : it was written just to make NN DFlow node serialization
						//work
						if(commands_partitioned.Length>1){
							Console.WriteLine("multiline command execution");
							object ret = null;
							foreach(string cmd in commands_partitioned) ret=this.Evaluate(cmd);
							return ret;
						}
                        
						string[] transformed_command = new string[]{};
                        
                        //validate
                        foreach (var C in this.callbackConstraint.Values)
                                if (!C.Validate (new string[]{command}))
                                        throw new WIODatasetLanguageException ("The command batch has not passed validation");
                        foreach (var T in this.callbackTransfers.Values) 
                                transformed_command = T.Transfer (new string[]{command});

                        
                        //return indexedCommands[0].Execute ();
                        object y = null;
                        foreach (string cmd in transformed_command) {
                                try {
                                        y = EvaluateDirectly (cmd);
                                } catch (Exception e) {
                                        Utils.MainLogger.Log (e.ToString ());
                                        y = null;
                                }
                        }
                        
                        //return the last one
                        return y;
                }
                
                
                /// <summary>
                /// Is the type basic (is exactly - should it be parsed futher when looking for embedded datasets
                /// </summary>
                public bool _IsBasicType (object x)
                {
						return x is String || x is Int64 || x is Char || x is Double || x is Boolean;
                }

		
				public Dictionary<string, IDataset> cached_dataset = new Dictionary<string, IDataset>();
		
                /// <summary>
                /// Evaluates recursively command without applying transfers etc
                /// </summary>
                /// <returns>
                /// Result (in most cases nothing, as it will be loading)
                /// </returns>
                /// <param name='command'>
                /// Command string
                /// </param>
                /// <param name='expect_dataset'>
                /// Expect_dataset flag, needed in recursion to solve
                /// ambiguity in ex. {“union” : { “param1”: {“dataset2”:[]}}}
                /// </param>
                public object EvaluateDirectly (string command)
                {
			
						//FIXME: caching works only for exactly same string representation, we need dataset getname working here, this workaround here
						//just kills all spaces...
			
						command = command.Trim();
						command = command.Replace(" ","");
						command = command.Replace("\n","");
				
                
						if(command.StartsWith(" ")) return this.EvaluateDirectly(command.Substring(1));
			
			
			
			
                        //invariant section
                        //should be executed only on non-basic types (basic = jvalue constructors)
                        //it should return dataset/string/int/chat/object[] and nothing else. not sure about that anyway 
                        //recursion note : it returns underlying .NET object that is appropriate translation 
                        Utils.MainLogger.Log ("Evaluating " + command);
						if (command.StartsWith ("$")) { //cache
				
							if(command[1]=='{' && !cached_dataset.ContainsKey(command)){
								Utils.MainLogger.Log ("Caching dataset "+command.Substring(1));
								IDataset created_dataset = (IDataset)this.EvaluateDirectly(command.Substring(1));
								this.cached_dataset[command.Substring(1)] = created_dataset;
								return this.cached_dataset[command.Substring(1)];
							}
				
						}	
                        if (command.StartsWith ("{")) { //creation mark in our data language
				
							Utils.MainLogger.Log ("Command = "+command+" checking in cache");
								if(cached_dataset.ContainsKey(command)) 
									return cached_dataset[command];
				
				
                                Utils.MainLogger.Log (">> Dataset");
                                Dictionary<string, object> parse_level = JsonConvert.DeserializeObject<Dictionary<string, object>> 
                                        (command);      
                                
                                //it can have a J
                                
                                string dataset_name = "";
                                
                                foreach (var x in parse_level.Keys) {
                                        dataset_name = x;
                                }
                                
                                object dataset = null;
                                
                                /* Try to construct dataset object */
                                try {
                                        dataset = this.callbackDataset [dataset_name].InvokeMember (
                                                null,
                                                BindingFlags.CreateInstance,
                                                null,
                                                null,
                                                null
                                        );
                                        
                                } catch (Exception e) {
                                        Utils.MainLogger.Log("Exception during creation dataset : " + e.ToString ());
                                }
                                
                                object param_object = parse_level [dataset_name];
        
                                
                                List<Tuple<string, object>> upd_params_generic = new List<Tuple<string,object>> ();
                        
// not supported
//                              if (param_object is JArray) {
//                                      JArray upd_params = (JArray)param_object;
//                                      for (int i = 0; i< upd_params.Count; ++i) {                                     
//                                              
//                                              
//                                              if (upd_params [i] is JValue) {
//                                                      upd_params_generic.Add (upd_params [i]);
//                                              } else {
//                                                      string param = upd_params [i].ToString ();
//                                                      Logger.Instance.log ("Parsing :" + param + "," + upd_params [i].GetType ().Name);
//                                                      upd_params_generic.Add (this.EvaluateDirectly (param));
//                                              }
//                                      }                                               
//                              } else {
                                Dictionary<string, object> param_dict = 
                                                JsonConvert.DeserializeObject<Dictionary<string,object>> (param_object.ToString ());
                                        
                                foreach (string x in param_dict.Keys) {
                                        Utils.MainLogger.Log  ("Parsing dicty :" + param_dict [x].ToString ());
                                        //check if it is convertible to JValue
                                        if (this._IsBasicType (param_dict [x])) {  //add more ?
                                                upd_params_generic.Add (new Tuple<string, object> (x, param_dict [x]));
                                        } else {
                                                upd_params_generic.Add (new Tuple<string, object> (x, this.EvaluateDirectly (param_dict [x].ToString ())));
                                        }
                                }
                                

								bool META_CACHE = false;
				
				
                                /* Load parameters recursively and when done call through reflection UpdateParameters */
                                try {   
                                        object parameters = ((IDataset)dataset).DefaultParameters ();
                                        /*Reflection magic goes here*/
//                                      ConstructorInfo ctor = parameters.GetType()
                                        Utils.MainLogger.Log  ("About to assign parameters");
                                        foreach (FieldInfo p in parameters.GetType().GetFields())
                                                Utils.MainLogger.Log  (">>>> " + p.Name);
                                        for (int i=0; i<upd_params_generic.Count; ++i) {
                                                Utils.MainLogger.Log  (upd_params_generic [i].ToString () + "," + upd_params_generic [i].GetType ().Name);
                                                
												if(upd_params_generic[i].Item1 == "WIO_CACHE_META"){
													META_CACHE = (bool)upd_params_generic[i].Item2;
													continue;
												}
						
												try {
                                                        FieldInfo p = parameters.GetType ().GetField (upd_params_generic [i].Item1.ToString ());
                                                        p.SetValue (parameters, upd_params_generic [i].Item2);
                                                        Utils.MainLogger.Log ("Successfully assigned "+upd_params_generic[i].Item1.ToString());
                                                } catch (Exception e) {
                                                        Utils.MainLogger.Log  ("Field not existing : "+e.ToString()); // i guess no other error is poss
                                                }
                                        }       
                                        Utils.MainLogger.Log  ("Successfully prepared parameters class");
                                        dataset.GetType ().GetMethod ("UpdateParameters").Invoke (dataset, new object[]{parameters});
                                } catch (Exception e) {
                                        Utils.MainLogger.Log  ("Exception during parameters object construction " + e.ToString ());     
                                }
                                
				
								if(META_CACHE){
								
									if(!cached_dataset.ContainsKey(command)){
										Utils.MainLogger.Log ("Caching dataset "+command);
										this.cached_dataset[command] = (IDataset)dataset;
									}
				
								}
				
				
				
                                return dataset;
                        } else { //it is not a dataset definitely, then we should process it futher
                                
                                object x = null;
                                try {
                                        if (command.StartsWith ("[")) {
                                                x = JsonConvert.DeserializeObject<object[]> (command);
                                        } else
                                                x = JsonConvert.DeserializeObject (command);
                                        
                                        if (this._IsBasicType (x))
                                                return x;
                                        else {
                                                object[] array = (object[])x;
                                                object[] array_dest = new object[array.Length];
                                                for (int i = 0; i < array.Length; ++i) {
                                                        if (!(this._IsBasicType (array [i]))) //investigate futher
                                                                array_dest [i] = this.EvaluateDirectly (array [i].ToString ());
                                                        else 
                                                                array_dest [i] = array [i];
                                                }
                                                return array_dest;
                                        }
                                } catch (Exception e) {
                                        Utils.MainLogger.Log  ("Probably illegal json (have you tried too much?) " + e.ToString ());
                                }
                                return null;
                        }
                }
                

                public static WIODatasetLanguageEnv Instance{
                        get{
                                // WARNING : not thread safe !
                                if(instance == null) instance = new WIODatasetLanguageEnv();
                                return instance;
                        }
                }
                
                /// <summary>
                /// Loads the assembly from file. In the assembly can be specified any number of Constraints, Commands, Transfers and Datasets. 
                /// Through reflection the assembly will be scanned and register every class that implements given interfaces and specifies attribute
                /// WIOCallAnnotation
                /// </summary>
                public void LoadAssembly (string path)
                {
						
                        try{
                                this.LoadAssembly(Assembly.LoadFrom (path));
                        }
                        catch(Exception e){
                                if(e is FileNotFoundException) Utils.MainLogger.Log("Wrong path name");
                                else Utils.MainLogger.Log(e.ToString());
                                //TODO: rsemove from loaded Assemblies (return status quo)                      
                        }
                }
                


                
                // ========== private methods (exposed because there is no friend mechanism in C#, at least I dont know how to do it)  ========= //
                public void LoadAssembly (Assembly assembly)
                {
                        try {
                                this.assemblies.Add (assembly);
			
                                foreach (Type t in assembly.GetTypes()) {
//										Console.WriteLine("tmpdebug : looking at {0}", t.ToString());
                                        System.Attribute[] attrs = System.Attribute.GetCustomAttributes (t); 
                                        foreach (var x in attrs) {
                                                if (x is WIOCallnameAnnotation) {
                                                        WIOCallnameAnnotation x_cast = (WIOCallnameAnnotation)x;
                                                        /* Check if already in the dictionary */
                                                        if (this.nameRegistrationDict.ContainsKey (x_cast.Name))
                                                                throw new WIODatasetLanguageException ("ERROR: Name registered in assembly " + assembly.FullName);
                                                        Utils.MainLogger.Log (" >> Loaded " + t.GetType ().AssemblyQualifiedName.ToString () + " as " + x_cast.Name);
                                                        
                                                        /* Do bookeeping */     
                                                        this.nameRegistrationDict [x_cast.Name] = true;
                                                                
                                                        object constructed_object = null;
                                                        /* Call constructors */
                                                        if (typeof(IDataset).IsAssignableFrom (t)) {
                                                                this.callbackDataset [x_cast.Name] = t; // no object construction because can be memory consuming       
                                                        } else if (typeof(ICommand).IsAssignableFrom (t)) {
                                                                constructed_object = t.InvokeMember (null, BindingFlags.CreateInstance, null, null, null);
                        
                                                                this.callbackCommands [x_cast.Name] = (ICommand)constructed_object;
                                                        } else if (typeof(ITransfer).IsAssignableFrom (t)) {
                                                                constructed_object = t.InvokeMember (null, BindingFlags.CreateInstance, null, null, null);
                                                                this.callbackTransfers [x_cast.Name] = (ITransfer)constructed_object;                                           
                                                        } else if (typeof(IConstraint).IsAssignableFrom (t)) {
                                                                constructed_object = t.InvokeMember (null, BindingFlags.CreateInstance, null, null, null);
                                                                this.callbackConstraint [x_cast.Name] = (IConstraint)constructed_object;                                                                
                                                        }
                                                                                                        
                                                        /* Check if it wants environment reference */
                                                        MethodInfo m = t.GetMethod ("SetEnvironment");
                                                        if (m != null) {
                                                                m.Invoke (constructed_object, new object[]{this});
                                                        }
                                                        
                                                }
                                        }
                                }
                                Utils.MainLogger.Log (">> Successfully loaded assembly "+ assembly.FullName);
                        }catch(Exception e){
                                if(e is FileNotFoundException) Utils.MainLogger.Log("Wrong path name");
                                else Utils.MainLogger.Log(e.ToString());
                                //TODO: remove from loaded Assemblies (return status quo)
                        }
                }       
                                
                public void RecursivelyLoadAssemblies (string path)
                {
					
						if(!Directory.Exists(path)) return;
			
                        foreach (string f in Directory.GetFiles(path))
                                if(f.EndsWith(".dll")) this.LoadAssembly (f);
                        foreach (string d in Directory.GetDirectories(path))
                                RecursivelyLoadAssemblies (d);
                }
        }
}