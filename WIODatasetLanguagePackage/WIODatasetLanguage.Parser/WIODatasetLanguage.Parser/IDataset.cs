using System;
using System.Collections;

namespace wideiodataparser {	
	//TODO: compile wideio-wideio and add as DLL
	
	/// <summary>
	/// Dataset core structure.
	/// </summary>
	public abstract class IDataset
	{

		
		protected object parameters;

		/// <summary>
		/// Ids collection.
		/// </summary>
		public abstract IEnumerable ids();

		/// <summary>
		/// Gets the element.
		/// </summary>
		/// <returns>
		/// The element.
		/// </returns>
		/// <param name='id'>
		/// Identifier.
		/// </param>
		public abstract object GetElement(object id);

//		/// <summary>
//		/// Gets the metadata label.
//		/// </summary>
//		/// <returns>
//		/// The metadata label.
//		/// </returns>
//		/// <param name='id'>
//		/// Identifier.
//		/// </param>
//		public abstract object GetMetadataLabel(object id);

		public abstract void UpdateParameters(object p); // TODO: add reflection code
		
		public virtual object DefaultParameters()
        {
		
            return this.GetType().GetNestedType("Parameters").InvokeMember(null, System.Reflection.BindingFlags.CreateInstance, null, null, null);
        }

		public object GetParameters()
        {
            if (parameters == null)
            {
                parameters = DefaultParameters();
            }
            return parameters;
        }
	}


	[AttributeUsage(AttributeTargets.Class)]
    public class DatasetAnnotation : System.Attribute
    {
        public DatasetAnnotation()
        {
        }
    }




}