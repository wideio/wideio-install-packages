using System;
using System.Reflection;
using NUnit;
using NUnit.Framework;

using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;

using System.Drawing;

using WIODataLanguage;

namespace WIODatasetLanguage
{

	
	public class MainClass
	{
	
			
		public static void Main (string[] args)
		{
	
			WIODatasetLanguageEnv.Instance.RecursivelyLoadAssemblies(".");
			WIODatasetLanguageEnv.Instance.EvaluateDirectly("{\"Bang\":{}}");
			
		}
		
		//TODO: Write unit tests! Including unit tests of all the small datasets utils (like shuffle)
	}
}; 
