using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;

namespace WIODataLanguage
{

	//TODO: GetFullyQualifiedName (as in Assembly) : can be useful for caching system

	//TODO: write documentation on how to write a dataset, emphasizing for instance that ID has to cloneable, and
	//metalabels should be nullable if you want to support subset metalabels
	
	
	public class WIODataLanguageException : Exception{
		public string exception;
		public WIODataLanguageException(string exception =  "WIODataLanguageException"){ this.exception = exception ; }
		public override string ToString ()
		{
			return this.exception;
		}
	}
	
	public class WIODataLanguageWrongFormatException : WIODataLanguageException{
		public WIODataLanguageWrongFormatException(string exception  = "WIODataLanguageWrongFormatException"): base(exception){
		}
	}
	
	
	public class TestDataset : IDataset{
		public override IEnumerable Keys ()
		{
			return null;
		}
		public override object GetElement(object id){
			return null;
		}
		public override void UpdateParameters (object p)
		{
			return;
		}
		
		public int GetMetadataID (object id)
		{
			return 0;
		}
		public string GetMetadataString(object id){
			return "hello";
		}
		
	}
	
	public class MainClass{
		public static void Main (string[] args)
		{
			/*Test GetAvailableMetadataFunctionsNames*/
			TestDataset td = new TestDataset ();
			foreach (var x in td.GetAvailableMetadata())
				Console.WriteLine (x.ToString ());
		}
	}
	
	
	/// <summary>
	/// Dataset core structure.
	/// </summary>
	public abstract class IDataset
	{
		protected object parameters;

		/// <summary>
		/// Ids collection. Note: It is imporant to keep ID easy and cloneable
		/// </summary>
		public abstract IEnumerable Keys();

		
		/// <summary>
		/// Gets the element.
		/// </summary>
		/// <returns>
		/// The element.
		/// </returns>
		/// <param name='id'>
		/// Identifier.
		/// </param>
		public abstract object GetElement(object id);

//		/// <summary>
//		/// Gets the metadata label.
//		/// </summary>
//		/// <returns>
//		/// The metadata label.
//		/// </returns>
//		/// <param name='id'>
//		/// Identifier.
//		/// </param>
//		public abstract object GetMetadataLabel(object id);

		
		/// <summary
		/// Updates the parameters. Has to be implement, it is the main construction method
		/// for dataset
		/// </summary>

		public abstract void UpdateParameters(object p); // TODO: add reflection code
		
		
		/// <summary>
		/// Gets the available metadata labels for given object
		/// </summary>
		/// <returns>
		/// The available metadata functions names
		/// </returns>
		/// <description>
		/// string[] to keep it simple for parser
		/// </description>
		public virtual string[] GetAvailableMetadata (object id = null) 
		{
			//Idea is that an object in the dataset can have only a subset of metalabels,
			//so GetAvailableMetadata is really dependent on object
			List<string> extracted_methods = new List<string> ();
			foreach (var method in this.GetType().GetMethods()) {
				if (method.Name.StartsWith ("GetMetadata"))
					extracted_methods.Add (method.Name.Substring("GetMetadata".Length));
			}
			return extracted_methods.ToArray ();
		}
		
		
		/// <summary>
		/// Gets the metadata by label. The idea is that some datasets are manipulating datasets, and making function GetMetada for every metadata would be quite complicated
		/// </summary>
		/// <returns>
		/// The metadata that is returned by function GetMetadata<NAME>
		/// </returns>
		public virtual object GetMetaByLabel (object id, string name)
		{
			//first check if there is a method GetMetadata<Name>
			//if not we have a problem.
			
			MethodInfo m = this.GetType ().GetMethod ("GetMetadata" + name);
			if (m == null)
				throw new WIODataLanguageException ("Metadata not present");
			
			return m.Invoke(this, new object[]{id});
		}
		
		
		
		/// <summary>
		/// Gets the number of elements in the dataset, warning : O(N) complexity (to keep the API simple)
		/// </summary>

		
		
		public virtual object DefaultParameters()
        {
		
            return this.GetType().GetNestedType("Parameters").InvokeMember(null, System.Reflection.BindingFlags.CreateInstance, null, null, null);
        }

		public object GetParameters()
        {
            if (parameters == null)
            {
                parameters = DefaultParameters();
            }
            return parameters;
        }
		
		public int Count {
			get {
				int return_value = 0;
				foreach (object x in this.Keys ())
					return_value ++;
				return return_value;
			}
		}	
		/// <summary>
		/// Removes  (or hide) elements, it doesn't have to be implemented. But you should
		/// implement it if your dataset when loaded to memory needs a lot of space
		/// (for instance you do heavy caching).
		/// 
		/// Imporant for exclude (for instance, not only)
		/// 
		/// In batch mode (many at once) to improve performance (problem with TestProd500000 from WIODataLanguageParser unit tests)
		/// 
		/// </summary>
		public virtual bool TryRemoveElementEntries (object[] ids)
		{
			return false;
		}

		public void Check(){
			return;
		}

		/// <summary>
		/// Gets the full name of the dataset. The invariant is that : if two datasets have got same full names
		/// they are identical and can be cached therefore
		/// </summary>
		public virtual string GetFullName(){
			return "IDataset";
		}

		/// <summary>
		/// For more complicated datasets (union(limit(...))) attributes are not enough to resolve GetElement Type
		/// For simple datasets it is enough and by default QueryGetElementType will just return the attribute
		/// </summary>
		/// <returns>
		/// GetElement returned type
		/// </returns>
		public virtual Type QueryGetElementType(){
			foreach(MethodInfo mi in this.GetType ().GetMethods()){
				object[] attributes = mi.GetCustomAttributes(false);
				foreach(object atr in attributes){
					if(atr is DatasetGetElementTypeAttribute){
						DatasetGetElementTypeAttribute dta = (DatasetGetElementTypeAttribute)atr;
						return dta.type;
					}
				}
			}

            return typeof(object);
		}
	}
	
	//for consistency with dataset are created (not static)
	public abstract class ITransfer{
		public abstract string[] Transfer(string[] input);
	}
	
	public abstract class IConstraint{
		public abstract bool Validate(string[] input);
	}
	
	public abstract class ICommand{
		//note: maybe add dependencies?
		/// <summary>
		/// Execute the command. If you need to call WIODataLanguageEnv for some reason , you can do it using reflection, but it shouldn't be neccesary. 
		/// </summary>
		/// <param name='parameters'>
		/// Parameters.
		/// </param>
		public abstract object Execute(params object[] parameters);
	}
	
	[AttributeUsage(AttributeTargets.Class)]
    public class WIOCallnameAnnotation : System.Attribute
    {
		public string Name;
        public WIOCallnameAnnotation (string Name = "")
		{
			this.Name = Name;
        }
    }


	
	[AttributeUsage(AttributeTargets.All)]
	public class DatasetGetElementTypeAttribute : System.Attribute
	{
		public Type type;
		public  DatasetGetElementTypeAttribute(Type t)
		{
			type=t;
		}
	}	
	
	[AttributeUsage(AttributeTargets.All)]
    public class DatasetTypeAttribute : System.Attribute
    {
        public Type type;
        public  DatasetTypeAttribute(Type t)
        {
			type=t;
        }
    }
}
