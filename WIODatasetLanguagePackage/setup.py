from cStringIO import StringIO
import os
import logging
import sys
import subprocess
from subprocess import Popen, PIPE, STDOUT

#TODO: add prefix support

def build():
    issueoutput("Cleaning previous installations ..")

    issuecommand("gacutil -u WIODataLanguageGlobals -gacdir "+os.environ['MONO_INSTALL_PATH'])
    issuecommand("mkdir -p /wideio/lib",True)
    issuecommand("rm /wideio/lib/WIODatasetLanguage.Parser.dll",True)
    issuecommand("rm /wideio/lib/WIODatasetLanguage.GenericDatasets.dll",True)
    issuecommand("rm /wideio/lib/WIODatasetLanguage.Globals.dll",True)
    issuecommand("rm WIODatasetLanguage.Globals/WIODatasetLanguage.Globals/obj -r -f",True)

    issueoutput("..Preparing installation")
    issuecommand("mkdir -p lib",True)

    issueoutput("..Building shared library for IDataset")
    issuecommand("sn -k WIODatasetLanguage.Globals/WIODatasetLanguage.Globals/WIODataLanguageGlobals.snk")
    issuecommand("xbuild /p:Configuration=Release WIODatasetLanguage.Globals/WIODatasetLanguage.Globals/WIODatasetLanguage.Globals.csproj")   
    
    issueoutput("..Registering shared library")
    issuecommand("sudo gacutil -i WIODatasetLanguage.Globals/WIODatasetLanguage.Globals/bin/Release/WIODataLanguageGlobals.dll -gacdir "+os.environ['MONO_INSTALL_PATH']+" -package WIODataLanguageGlobals")
    issuecommand("mkdir -p /usr/lib/cli/WIODataLanguageGlobals/",True)
    issuecommand("ln -s "+os.environ['MONO_INSTALL_PATH']+"/lib/mono/WIODataLanguageGlobals/WIODataLanguageGlobals.dll /usr/lib/cli/WIODataLanguageGlobals/", True)
    issuecommand("mv WIODataLanguageGlobals.pc /usr/lib/pkgconfig/",True)

    issueoutput("..Building other projects")
    prj_name = "WIODatasetLanguage.GenericDatasets"
    # It is on purpose sudo not sudo -E to get rid of messing env variables
    issuecommand("rm -r -f "+prj_name+"/"+prj_name+"/obj")
    issuecommand("sudo xbuild /p:Configuration=Release "+prj_name+"/"+prj_name+"/"+prj_name+".csproj")   
    issuecommand("mv "+prj_name+"/"+prj_name+"/bin/Release/"+prj_name+".dll lib")
    prj_name = "WIODatasetLanguage.Parser"
    issuecommand("rm -r -f "+prj_name+"/"+prj_name+"/obj")
    issuecommand("sudo xbuild /p:Configuration=Release "+prj_name+"/"+prj_name+"/"+prj_name+".csproj")   
    issuecommand("mv "+prj_name+"/"+prj_name+"/bin/Release/"+prj_name+".dll lib")

def install():
    issuecommand("rm /wideio/lib/WIODatasetLanguage.Parser.dll",True)
    issuecommand("rm /wideio/lib/WIODatasetLanguage.GenericDatasets.dll",True)
    issuecommand("rm /wideio/lib/WIODatasetLanguage.Globals.dll",True)
 
    issueoutput("..Linking with platform")
    path = os.getcwd()
    issuecommand("ln -s "+path+"/lib/WIODatasetLanguage.GenericDatasets.dll /wideio/lib/WIODatasetLanguage.GenericDatasets.dll")
    issuecommand("ln -s "+path+"/lib/WIODatasetLanguage.Parser.dll /wideio/lib/WIODatasetLanguage.Parser.dll")

def changedir(dir):
    os.chdir("/")

def issueoutput(message):
    print "WIODatasetPackage: "+message

def issuecommand(command, fail = False):
    p = Popen(command,shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
    output = p.communicate() 
    if fail == True and p.returncode != 0:
        print str(command) + "\t*SILENTLY FAILED*\t"
        print str(output)
        print ".... WARNING: silent fail"
    elif fail == False and p.returncode != 0:
        print str(command) + "\t*FAILED*\t"
        print str(output)
        raise Exception("Failed command "+str(command))
        

def itercommand(command):
    """ Util function to iterate through command output, command is array, ex. ["ls","arg1","arg2"] """
    output = subprocess.check_output(command)
    strio = StringIO(output)
    while True:
        n1 = strio.readline()
        if n1 == "" : raise StopIteration # Seriously.. how to do it if I have empty lines?
        yield n1.strip()




if __name__ == "__main__":
    if sys.argv[1]=="build" : build()    
    if sys.argv[1]=="install": install()
