using System;
using System.Collections.Generic;
using System.Collections;
using WIODataLanguage;
using System.IO;
using System.Drawing;

namespace WIO.DatasetLanguage
{
	/// <summary>
	/// From request dataset - a stub for future dataset doing superglue magic. 
	/// For now it assumes that the file is image
	/// It scans for all files with .input extension
	/// </summary>
	[WIOCallnameAnnotation("From_request")]
	public class FromRequestDataset : IDataset
	{

		
		public class Parameters{
			public string root_path = "."; // by default in the same folder
		}
		
		[WIODataLanguage.DatasetGetElementType(typeof(System.Drawing.Image))]     
		public override object GetElement (object id)
		{
			string casted_id = (string)id;
			if(!this.idDict.ContainsKey(casted_id)) throw new WIODataLanguageException("ID not present in the dataset");
			return System.Drawing.Image.FromFile(this.idDict[casted_id].physical_path);
		}
		
		public override void UpdateParameters(object p){
			this.parameters = (Parameters)p; 
			Parameters casted_parameters = (Parameters)p;
			
			foreach (string f in Directory.GetFiles(casted_parameters.root_path)) {
				if(f.EndsWith(".input")){
					DatasetEntry ds = new DatasetEntry{ID = f, physical_path = f};
					this.dataElements.Add(ds);
					this.idDict[f] = ds;
				}
			}
		}	
		
		public override IEnumerable Keys ()
		{
			foreach(var x in dataElements){
				yield return x.ID.Clone();
			}
		}
		
		
		
		protected List<DatasetEntry> dataElements =new List<DatasetEntry>();
		protected Dictionary<string, DatasetEntry> idDict = new Dictionary<string, DatasetEntry>();		
		
		public class DatasetEntry{
			// ID is simply a list of string
			public string ID;
			public string physical_path;
		}		
		public FromRequestDataset (){		
		}		
	}
}

