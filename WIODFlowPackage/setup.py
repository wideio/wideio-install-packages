from cStringIO import StringIO
import os
import logging
import sys
import subprocess
from subprocess import Popen, PIPE, STDOUT

#TODO: add prefix support

def build():
    issueoutput("..Preparing")
    path = os.getcwd()
    issuecommand("cp /wideio/lib/WIODatasetLanguage.Parser.dll "+path+"/lib")
    issuecommand("rm -f -r wideio-dflow/obj",True)
    issuecommand("rm -f -r WIODFlowCommandTool/WIODFlowCommandTool/obj",True)
    issuecommand("mkdir lib",True)

    issueoutput("..Building other projects")
    # It is on purpose sudo not sudo -E to get rid of messing env variables
    issuecommand("sudo xbuild /p:Configuration=Release wideio-dflow/DFlowCore.csproj")
    issuecommand("mv wideio-dflow/bin/Release/DFlowCore.dll lib/")

    issuecommand("sudo xbuild /p:Configuration=Release WIODFlowCommandTool/WIODFlowCommandTool/WIODFlowCommandTool.csproj")


def install():
    path = os.getcwd()
    issuecommand("ln -s "+path+"/lib/DFlowCore.dll /wideio/lib/DFlowCore.dll",True)
    issuecommand("cp wio_dflow /wideio/bin/")
    issuecommand("rm /wideio/bin/wio_dflow.exe",True)
    issuecommand("ln -s "+path+"/WIODFlowCommandTool/WIODFlowCommandTool/bin/Release/WIODFlowCommandTool.exe /wideio/bin")
    issuecommand("mv /wideio/bin/WIODFlowCommandTool.exe /wideio/bin/wio_dflow.exe")

def changedir(dir):
    os.chdir("/")

def issueoutput(message):
    print "WIODFlowPackage: "+message

def issuecommand(command, fail = False):
    p = Popen(command,shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
    output = p.communicate() 
    if fail == True and p.returncode != 0:
        print str(command) + "\t*SILENTLY FAILED*\t"
        print str(output)
        print ".... WARNING: silent fail"
    elif fail == False and p.returncode != 0:
        print str(command) + "\t*FAILED*\t"
        print str(output)
        raise Exception("Failed command "+str(command))
    print str(command)
        

def itercommand(command):
    """ Util function to iterate through command output, command is array, ex. ["ls","arg1","arg2"] """
    output = subprocess.check_output(command)
    strio = StringIO(output)
    while True:
        n1 = strio.readline()
        if n1 == "" : raise StopIteration # Seriously.. how to do it if I have empty lines?
        yield n1.strip()




if __name__ == "__main__":
    if sys.argv[1]=="build" : build()    
    if sys.argv[1]=="install": install()
